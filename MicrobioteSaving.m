%%======================================================================%%
% 		Matlab code associated to the publication		%% 
%									%%
%      	A mathematical model to investigate the key drivers of		%%
%	the biogeography of the colon microbiota.     			%%
%       Authors : Simon Labarthe ,Bastien Polizzi, Thuy Phan, 		%%
%	Thierry Goudon, Magali Ribot, Beatrice Laroche    		%%
%                      					                %%
%%======================================================================%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Content: saving routines 			 %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Display informations on the time loop :
disp(['********************  SAVE NUMBER : ',num2str(SaveNumber),' ********************'])
disp(['Current time in days : ',num2str(t)])
disp(['Number of time step  : ',num2str(StepNumber,'%10.2e')])
disp(['Number of save       : ',num2str(SaveNumber,'%10.2e')])
disp(char(10))  % Line break

% Save intermediary data in Matlab format :
FileName=['Microbiote_T',num2str(TestNumber),'_step=',num2str(SaveNumber),'.mat'];
name=[PathName,FileName];
if exist('Uzsimp')
    if t>=Tf
        save(name,'C','Ur','Uz','Uzsimp','Ursimp','Vchemotactic_r','Vchemotactic_z','SumComp','t','TimeLine','StepNumber');
    else
        save(name,'C','Ur','Uz','Uzsimp','Ursimp','Vchemotactic_r','Vchemotactic_z','t','StepNumber');
    end
else
    if exist('S')
        if t>=Tf
            save(name,'C','S','Ur','Uz','Vchemotactic_r','Vchemotactic_z','t','StepNumber');
        else
            save(name,'C','S','Ur','Uz','Vchemotactic_r','Vchemotactic_z','t','StepNumber');
        end
    else
        if t>=Tf
            save(name,'C','Ur','Uz','Vchemotactic_r','Vchemotactic_z','SumComp','t','TimeLine','StepNumber');
        else
            save(name,'C','Ur','Uz','Vchemotactic_r','Vchemotactic_z','t','StepNumber');
        end
    end
end
%-------------------------------------------------------------------------%
% END :                 Save of the results in 2D in .mat                 %
%-------------------------------------------------------------------------%

