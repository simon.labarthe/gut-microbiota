%%======================================================================%%
% 		Matlab code associated to the publication		%% 
%									%%
%      	A mathematical model to investigate the key drivers of		%%
%	the biogeography of the colon microbiota.     			%%
%       Authors : Simon Labarthe ,Bastien Polizzi, Thuy Phan, 		%%
%	Thierry Goudon, Magali Ribot, Beatrice Laroche    		%%
%                      					                %%
%%======================================================================%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Content: Post-processing routines					%	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%% Flag to save the output on vtk, or to radially average the output
Compute_VTK=1
Compute_MeanR=1



% Initialization :
%------------------------------------------
if exist('S')
    Variable_Average = zeros(Nb.phases+Nb.dissolved+1,NbSaving+1); % plus one for time
    Variable_Average_Prox_Lum = zeros(Nb.phases+Nb.dissolved+1,NbSaving+1); % plus one for time
    Variable_Average_Prox_Muc = zeros(Nb.phases+Nb.dissolved+1,NbSaving+1); % plus one for time
    Variable_Average_Trans_Lum = zeros(Nb.phases+Nb.dissolved+1,NbSaving+1); % plus one for time
    Variable_Average_Trans_Muc = zeros(Nb.phases+Nb.dissolved+1,NbSaving+1); % plus one for time
    Variable_Average_Dist_Lum = zeros(Nb.phases+Nb.dissolved+1,NbSaving+1); % plus one for time
    Variable_Average_Dist_Muc = zeros(Nb.phases+Nb.dissolved+1,NbSaving+1); % plus one for time
    SCFA_Water_Flux = zeros(Nb.SCFA+2,NbSaving+1); % plus one for water, plus one for time
else
    Variable_Average = zeros(Nb.phases+1,NbSaving+1); % plus one for time
    Variable_Average_Prox_Lum = zeros(Nb.phases+Nb.dissolved+1,NbSaving+1); % plus one for time
    Variable_Average_Prox_Muc = zeros(Nb.phases+Nb.dissolved+1,NbSaving+1); % plus one for time
    Variable_Average_Trans_Lum = zeros(Nb.phases+Nb.dissolved+1,NbSaving+1); % plus one for time
    Variable_Average_Trans_Muc = zeros(Nb.phases+Nb.dissolved+1,NbSaving+1); % plus one for time
    Variable_Average_Dist_Lum = zeros(Nb.phases+Nb.dissolved+1,NbSaving+1); % plus one for time
    Variable_Average_Dist_Muc = zeros(Nb.phases+Nb.dissolved+1,NbSaving+1); % plus one for time
end


if Compute_MeanR==1
   Variable_Average_R_Uz = zeros(Nz+1,NbSaving+2);%plus one for time, plus one for z
   Variable_Average_R_Uz(2:Nz+1,1)=linspace(dz,Lgut-dz,Nz);
   Variable_Average_R_Ur = zeros(Nz+1,NbSaving+2);%plus one for time, plus one for z
   Variable_Average_R_Ur(2:Nz+1,1)=linspace(dz,Lgut-dz,Nz);
   Variable_Average_R_TotalBact = zeros(Nz+1,NbSaving+2);%plus one for time, plus one for z
   Variable_Average_R_TotalBact(2:Nz+1,1)=linspace(dz,Lgut-dz,Nz); 
   Variable_Average_R_LBoundary = zeros(Nz+1,NbSaving+2);%plus one for time, plus one for z
   Variable_Average_R_LBoundary(2:Nz+1,1)=linspace(dz,Lgut-dz,Nz); 
   Variable_Average_R_Fibre = zeros(Nz+1,NbSaving+2);%plus one for time, plus one for z
   Variable_Average_R_Fibre(2:Nz+1,1)=linspace(dz,Lgut-dz,Nz);
   Variable_Average_Total_Bact_Activity = zeros(Nz+1,NbSaving+2);%plus one for time, plus one for z
   Variable_Average_Total_Bact_Activity(2:Nz+1,1)=linspace(dz,Lgut-dz,Nz);
   Variable_Average_Viscosity = zeros(Nz+1,NbSaving+2);%plus one for time, plus one for z
   Variable_Average_Viscosity(2:Nz+1,1)=linspace(dz,Lgut-dz,Nz);
   Variable_Average_Bound_Ur = zeros(Nz+1,NbSaving+2);%plus one for time, plus one for z
   Variable_Average_Bound_Ur(2:Nz+1,1)=linspace(dz,Lgut-dz,Nz);    
end

% Computing domain and subdomain surfaces :
%------------------------------------------
Nr_Lumen = floor(Rgut_Lumen/dr);
Nz_Prox = floor(Lgut_Prox/dz);
Nz_Trans = floor(Lgut_Prox/dz);
Surface=sum(sum(ones(Nz,Nr).*ElementaryVolume));
Surface_Prox_Lum=sum(sum(ones(Nz_Prox,Nr_Lumen).*ElementaryVolume(1:Nz_Prox,1:Nr_Lumen)));
Surface_Trans_Lum=sum(sum(ones(Nz_Trans,Nr_Lumen).*ElementaryVolume(Nz_Prox+1:Nz_Prox+Nz_Trans,1:Nr_Lumen)));
Surface_Dist_Lum=sum(sum(ones(Nz-(Nz_Prox+Nz_Trans),Nr_Lumen).*ElementaryVolume(Nz_Prox+Nz_Trans+1:Nz,1:Nr_Lumen)));
Surface_Prox_Muc=sum(sum(ones(Nz_Prox,Nr-Nr_Lumen).*ElementaryVolume(1:Nz_Prox,Nr_Lumen+1:Nr)));
Surface_Trans_Muc=sum(sum(ones(Nz_Trans,Nr-Nr_Lumen).*ElementaryVolume(Nz_Prox+1:Nz_Prox+Nz_Trans,Nr_Lumen+1:Nr)));
Surface_Dist_Muc=sum(sum(ones(Nz-(Nz_Prox+Nz_Trans),Nr-Nr_Lumen).*ElementaryVolume(Nz_Prox+Nz_Trans+1:Nz,Nr_Lumen+1:Nr)));


% Post-processing each time step :
%------------------------------------------
for i = 0:NbSaving
    Variable_Average(1,i+1) = i*SaveInterval;
    SCFA_Water_Flux(1,i+1) = i*SaveInterval;
    FileName=['Microbiote_T',num2str(TestNumber),'_step=',num2str(i),'.mat'];
    name=[PathName,FileName];
    load(name);
    Output_Mesh_Param = [[Nr,Nz,1],[dr,dz,0]];
    for k=1:Nb.phases
        FileName=['Microbiote_T',num2str(TestNumber),'_phase=',ID.phase_idx{k},'_step=',num2str(i),'.vtk'];
        name=[PathName,FileName];
        current_Xi = C(:,:,k);
	%%%%% Computing the vtk file=> display with paraview
	if Compute_VTK==1
        ToVTK(name,Output_Mesh_Param,ID.phase_idx{k},current_Xi);
	end
	%%%%% Computing averaged values
        Variable_Average(k+1,i+1)=sum(sum(current_Xi.*ElementaryVolume))/Surface;
	Variable_Average_Prox_Lum(k+1,i+1)=sum(sum(current_Xi(1:Nz_Prox,1:Nr_Lumen).*ElementaryVolume(1:Nz_Prox,1:Nr_Lumen)))/Surface_Prox_Lum;
        Variable_Average_Prox_Muc(k+1,i+1)=sum(sum(current_Xi(1:Nz_Prox,Nr_Lumen+1:Nr).*ElementaryVolume(1:Nz_Prox,Nr_Lumen+1:Nr)))/Surface_Prox_Muc;
    	Variable_Average_Trans_Lum(k+1,i+1) = sum(sum(current_Xi(Nz_Prox+1:Nz_Trans+Nz_Prox,1:Nr_Lumen).*ElementaryVolume(Nz_Prox+1:Nz_Trans+Nz_Prox,1:Nr_Lumen)))/Surface_Trans_Lum;
	Variable_Average_Trans_Muc(k+1,i+1) = sum(sum(current_Xi(Nz_Prox+1:Nz_Trans+Nz_Prox,Nr_Lumen+1:Nr).*ElementaryVolume(Nz_Prox+1:Nz_Trans+Nz_Prox,Nr_Lumen+1:Nr)))/Surface_Trans_Muc;
    	Variable_Average_Dist_Lum(k+1,i+1) = sum(sum(current_Xi(Nz_Trans+Nz_Prox+1:Nz,1:Nr_Lumen).*ElementaryVolume(Nz_Trans+Nz_Prox+1:Nz,1:Nr_Lumen)))/Surface_Dist_Lum;
	Variable_Average_Dist_Muc(k+1,i+1) = sum(sum(current_Xi(Nz_Trans+Nz_Prox+1:Nz,Nr_Lumen+1:Nr).*ElementaryVolume(Nz_Trans+Nz_Prox+1:Nz,Nr_Lumen+1:Nr)))/Surface_Dist_Muc;
    end
    if exist('S')
      for k=1:Nb.dissolved
        FileName=['Microbiote_T',num2str(TestNumber),'_solute=',ID.dissolved_idx{k},'_step=',num2str(i),'.vtk'];
        name=[PathName,FileName];
        current_Xi = S(:,:,k);
	%%%%% Computing the vtk file=> display with paraview
	if Compute_VTK==1
        ToVTK(name,Output_Mesh_Param,ID.dissolved_idx{k},current_Xi);
	end
	%%%%% Computing averaged values
        Variable_Average(k+Nb.phases+1,i+1)=sum(sum(current_Xi.*ElementaryVolume))/Surface;
	Variable_Average_Prox_Lum(k+Nb.phases+1,i+1)=sum(sum(current_Xi(1:Nz_Prox,1:Nr_Lumen).*ElementaryVolume(1:Nz_Prox,1:Nr_Lumen)))/Surface_Prox_Lum;
        Variable_Average_Prox_Muc(k+Nb.phases+1,i+1)=sum(sum(current_Xi(1:Nz_Prox,Nr_Lumen+1:Nr).*ElementaryVolume(1:Nz_Prox,Nr_Lumen+1:Nr)))/Surface_Prox_Muc;
    	Variable_Average_Trans_Lum(k+Nb.phases+1,i+1) = sum(sum(current_Xi(Nz_Prox+1:Nz,1:Nr_Lumen).*ElementaryVolume(Nz_Prox+1:Nz,1:Nr_Lumen)))/Surface_Trans_Lum;
	Variable_Average_Trans_Muc(k+Nb.phases+1,i+1) = sum(sum(current_Xi(Nz_Prox+1:Nz,Nr_Lumen+1:Nr).*ElementaryVolume(Nz_Prox+1:Nz,Nr_Lumen+1:Nr)))/Surface_Trans_Muc;
    	Variable_Average_Dist_Lum(k+Nb.phases+1,i+1) = sum(sum(current_Xi(Nz_Trans+Nz_Prox+1:Nz,1:Nr_Lumen).*ElementaryVolume(Nz_Trans+Nz_Prox+1:Nz,1:Nr_Lumen)))/Surface_Dist_Lum;
	Variable_Average_Dist_Muc(k+Nb.phases+1,i+1) = sum(sum(current_Xi(Nz_Trans+Nz_Prox+1:Nz,Nr_Lumen+1:Nr).*ElementaryVolume(Nz_Trans+Nz_Prox+1:Nz,Nr_Lumen+1:Nr)))/Surface_Dist_Muc;
      end
      for k=1:Nb.SCFA %SCFA flux
        current_Xi = S(:,:,ID.SCFA_in_dissolved(k));
        SCFA_Water_Flux(k+1,i+1)=sum(Bound.GammaM.(Bound.la_SCFA{k})(current_Xi(:,Nr)))*dz*2*pi*Rgut;
      end
      current_Xi = C;%(:,:,ID.l);%water
      SCFA_Water_Flux(5,i+1)=sum(Bound.GammaM.('l')(current_Xi))*dz*2*pi*Rgut;
    end

    %%%% Computing speed fields
    Output_Mesh_Param = [[Nr+1,Nz,1],[dr,dz,0]];
    FileName=['Microbiote_T',num2str(TestNumber),'_Ur_step=',num2str(i),'.vtk'];
    name=[PathName,FileName];
    ToVTK(name,Output_Mesh_Param,'Ur',Ur);
    Output_Mesh_Param = [[Nr,Nz+1,1],[dr,dz,0]];
    FileName=['Microbiote_T',num2str(TestNumber),'_Uz_step=',num2str(i),'.vtk'];
    name=[PathName,FileName];
    ToVTK(name,Output_Mesh_Param,'Uz',Uz);
    FileName=['Microbiote_T',num2str(TestNumber),'_VectU_step=',num2str(i),'.vtk'];
    name=[PathName,FileName];
    UzTemp = (Upar.VzToPGrid*Uz)';
    UrTemp = (Ur*Upar.VrToPGrid)';  

   %%%% Computation of averaged values 
   if Compute_MeanR==1
	%Computation of averaged speeds
        Variable_Average_R_Uz(1,i+2) = i*SaveInterval;
	VarTemp = (Upar.VzToPGrid*Uz);
        Variable_Average_R_Uz(2:Nz+1,i+2)=sum(VarTemp.*ElementaryVolume,2)/(dz*pi*Rgut^2);
        Variable_Average_R_U(1,i+2) = i*SaveInterval;
	VarTemp = (Ur*Upar.VrToPGrid);
        Variable_Average_R_Ur(2:Nz+1,i+2)=sum(VarTemp.*ElementaryVolume,2)/(dz*pi*Rgut^2);
	%Computation of averaged total bacteria
        Variable_Average_R_TotalBact(1,i+2) = i*SaveInterval;
	VarTemp = sum(C(:,:,ID.bacteria_idx_in_phase),3);
   	Variable_Average_R_TotalBact(2:Nz+1,i+2) = sum(VarTemp.*ElementaryVolume,2)/(dz*pi*Rgut^2);
	%Computation of liquid at the boundary
        Variable_Average_R_LBoundary(1,i+2) = i*SaveInterval;
   	Variable_Average_R_LBoundary(2:Nz+1,i+2) = C(:,Nr,ID.l);
	%Computation of averaged fibre levels
   	Variable_Average_R_Fibre(1,i+2) = i*SaveInterval;
	VarTemp = C(:,:,ID.pol);
        Variable_Average_R_Fibre(2:Nz+1,i+2)=sum(VarTemp.*ElementaryVolume,2)/(dz*pi*Rgut^2);
	%Computation of viscosity
   	Variable_Average_Viscosity(1,i+2) = i*SaveInterval;
	VarTemp = Upar.Mu(C,S);
        Variable_Average_Viscosity(2:Nz+1,i+2)=sum(VarTemp.*ElementaryVolume,2)/(dz*pi*Rgut^2);
	%Computation of the speed on gamma m (equal to total net flux through the mucosa).
	Variable_Average_Bound_Ur(1,i+2) = i*SaveInterval;
	Variable_Average_Bound_Ur(2:Nz+1,i+2)=Bound.GammaM.Vr(Bound.GammaM.l,Bound.GammaM.m,Bound.GammaM.pol,Bound.GammaM.bmon,Bound.GammaM.bla,Bound.GammaM.bH2a,Bound.GammaM.bH2m,Bound.GammaM.r,C,Ur);

	% Computation of the functional activity
   	Variable_Average_Total_Bact_Activity(1,i+2) = i*SaveInterval;
	rho_ce = zeros(Nz,Nr,Nb.process);
	rho_ci = zeros(Nz,Nr,Nb.process);
	rho_si = zeros(Nz,Nr,Nb.process);
	rho_se = zeros(Nz,Nr,Nb.process);
    
    
    	%%%% Computation of the commun basis for the different versions of rho
    	rho_tmp_P1_P6 = zeros(Nz,Nr,6);
    	rho_tmp_P1_P6(:,:,ID.P1) = C(:,:,KRpar.Idx_rho_i_num(1))./(KRpar.KxsP(1)*C(:,:,ID.bmon)+C(:,:,KRpar.Idx_rho_i_den(1))+KRpar.epsilon_KR); %KRpar.epsilon_KR=1e-12 is a little parameter to avoid singularities and Na
    	rho_tmp_P1_P6(:,:,ID.P2) = C(:,:,KRpar.Idx_rho_i_num(2))./(KRpar.KxsP(2)*C(:,:,ID.bmon)+C(:,:,KRpar.Idx_rho_i_den(2))+KRpar.epsilon_KR);
    	for Pidx=3:6
        	rho_tmp_P1_P6(:,:,Pidx) = C(:,:,KRpar.Idx_rho_i_num(Pidx))./(KRpar.KxsP(Pidx)+S(:,:,KRpar.Idx_rho_i_den(Pidx)));%.*(C(:,:,ID.m)+C(:,:,ID.l)+C(:,:,ID.pol)); % logistic growth term
    	end
    	rho_tmp_P1_P6(:,:,6) = rho_tmp_P1_P6(:,:,6).* KRpar.Ph_inib(:,:);
    
    	%%%%% Computation of rho_ci
    	rho_ci(:,:,ID.P1) = rho_tmp_P1_P6(:,:,ID.P1);
    	rho_ci(:,:,ID.P2) = rho_tmp_P1_P6(:,:,ID.P2);
    	for Pidx=3:6
        	rho_ci(:,:,Pidx) = rho_tmp_P1_P6(:,:,Pidx).*S(:,:,KRpar.Idx_rho_e_mul(Pidx));
    	end
    	for Pidx=7:10
        	rho_ci(:,:,Pidx) = 1;
    	end
    
    	%%%%% Computation of rho_ce
    	for Pidx=3:6
        	rho_ce(:,:,Pidx) = rho_ci(:,:,Pidx).*C(:,:,ID.l);
    	end
          
    	%% Scattering of tensors rho
    	rho_ci_scattered = zeros(Nz*Nr,Nb.process);
    	rho_ce_scattered = zeros(Nz*Nr,Nb.process);
    	for j=1:Nb.process
        	rho_ci_scattered(:,j) = reshape(rho_ci(:,:,j),Nz*Nr,1);
        	rho_ce_scattered(:,j) = reshape(rho_ce(:,:,j),Nz*Nr,1);
    	end
    
    	% Multiplication with reacting rates
    	Pci_times_rho_ci_scattered = rho_ci_scattered*KRpar.Pci';
    	Pce_times_rho_ce_scattered = rho_ce_scattered*KRpar.Pce';
    
    	%% Gathering of tensors P*_times_rho_*    
    	Pce_times_rho_ce = zeros(Nz,Nr,Nb.phases);
    	Pci_times_rho_ci = zeros(Nz,Nr,Nb.phases);
    	for j=1:Nb.phases
        	Pce_times_rho_ce(:,:,j) = reshape(Pce_times_rho_ce_scattered(:,j),Nz,Nr);
        	Pci_times_rho_ci(:,:,j) = reshape(Pci_times_rho_ci_scattered(:,j),Nz,Nr);
    	end

	for j=ID.bmon:ID.bH2m
		Pci_times_rho_ci(:,:,j)=Pci_times_rho_ci(:,:,j).*C(:,:,j);
	end
	VarTemp = sum(Pce_times_rho_ce(:,:,ID.bacteria_idx_in_phase),3)-sum(Pci_times_rho_ci(:,:,ID.bacteria_idx_in_phase),3);
        Variable_Average_Total_Bact_Activity(2:Nz+1,i+2)=sum(VarTemp.*ElementaryVolume,2)/(dz*pi*Rgut^2); 
	disp([num2str(i),' ',num2str(max(Variable_Average_Total_Bact_Activity(2:Nz+1,i+2)))]) 
   end 


end
FileName=['Microbiote_T',num2str(TestNumber),'_Variable_Radial_Average.mat'];
Variable_label={'time'};
for i=1:Nb.phases
    Variable_label{i+1}=ID.phase_idx{i};
end
if exist('S')
    for i=1:Nb.dissolved
    Variable_label{i+1+Nb.phases}=ID.dissolved_idx{i};
    end
    SCFA_label={'time'};
    for i=1:Nb.SCFA
        SCFA_label{i+1}=ID.SCFA{i};
    end
    SCFA_label{Nb.SCFA+2}={'l'};
    Int_Time = ones(NbSaving+1,NbSaving+1);
    Int_Time=triu(Int_Time);
    Int_Time_SCFA_flux = SaveInterval*SCFA_Water_Flux*Int_Time; %time integration
    Int_Time_SCFA_flux(1,:)=SCFA_Water_Flux(1,:); % restore time
end


% Extracting current values of each phase and solutes :
%------------------------------------------------------
bmon = C(:,:,ID.bmon)
pol = C(:,:,ID.pol)
mon = S(:,:,ID.mon)
bla = C(:,:,ID.bla)
bH2a = C(:,:,ID.bH2a)
bH2m = C(:,:,ID.bH2m)
la = S(:,:,ID.la)
H2 = S(:,:,ID.H2)





% Saving :
%---------

if Compute_MeanR==1
save([PathName,FileName],'Variable_Average','Variable_Average_Prox_Lum','Variable_Average_Prox_Muc','Variable_Average_Trans_Lum','Variable_Average_Trans_Muc','Variable_Average_Dist_Lum','Variable_Average_Dist_Muc','Variable_label','SCFA_label','SCFA_Water_Flux','Int_Time_SCFA_flux','Variable_Average_R_TotalBact','Variable_Average_R_Uz','Variable_Average_R_Ur','Variable_Average_R_LBoundary','Variable_Average_R_Fibre','Variable_Average_Total_Bact_Activity','Variable_Average_Viscosity','Variable_Average_Bound_Ur','Ur','Uz','bmon','pol','bla','mon','la','H2','bH2a','bH2m');    
else
save([PathName,FileName],'Variable_Average','Variable_Average_Prox_Lum','Variable_Average_Prox_Muc','Variable_Average_Trans_Lum','Variable_Average_Trans_Muc','Variable_Average_Dist_Lum','Variable_Average_Dist_Muc','Variable_label','SCFA_label','SCFA_Water_Flux','Int_Time_SCFA_flux');    
end
