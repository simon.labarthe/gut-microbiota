%%======================================================================%%
% 		Matlab code associated to the publication		%% 
%									%%
%      	A mathematical model to investigate the key drivers of		%%
%	the biogeography of the colon microbiota.     			%%
%       Authors : Simon Labarthe ,Bastien Polizzi, Thuy Phan, 		%%
%	Thierry Goudon, Magali Ribot, Beatrice Laroche    		%%
%                      					                %%
%%======================================================================%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Content: Saving data to vtk						%	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [ ] = ToVTK(Output_filename, Output_Mesh_Param,Data_Name,Data)
% WriteToVTK:  Write a mesh and associated data arrays to a vtk file.

% Arguments :
%   - Output_filename            : name of the output file (string).
%   - Output_Mesh_Param          : array [[nx,ny,nz],[dx,dy,dz]] where nx (resp. ny, nz) 
%				   and dx (resp. dy, dz) are the number of point and the space step in the x 
%				 direction (resp. y, z directions).
%   - Data_Name			 : name of the data field
%   - Data			 : raw data (array of nx*ny*nz dimension)
Output_file = fopen(Output_filename,'w');
% VTK file header
% VTK DataFile version
fprintf(Output_file, '# vtk DataFile Version 2.0\n');
% File Title
fprintf(Output_file, 'Matlab Data to VTK\n');
% Data format => BINARY
fprintf(Output_file, 'BINARY\n'); 
% Mesh Specification
fprintf(Output_file, 'DATASET STRUCTURED_POINTS\n');
fprintf(Output_file, ['DIMENSIONS ' num2str(Output_Mesh_Param(1)) ' ' num2str(Output_Mesh_Param(2)) ' ' num2str(Output_Mesh_Param(3)) '\n']);
Nb_Pts=Output_Mesh_Param(1)*Output_Mesh_Param(2)*Output_Mesh_Param(3);
%        Cells=sparse(zeros((Settings(1)-1)*(Settings(2)-1)*(Settings(3)-1),1));
fprintf(Output_file, 'ORIGIN 0.0 0.0 0.0\n');
fprintf(Output_file, ['SPACING ' num2str(Output_Mesh_Param(4)) ' ' num2str(Output_Mesh_Param(5)) ' ' num2str(Output_Mesh_Param(6)) '\n']);
% Write Data
fprintf(Output_file, ['POINT_DATA ',num2str(Nb_Pts),'\n']);    
fprintf(Output_file, ['SCALARS ', Data_Name,' float 1\n']);
fprintf(Output_file, 'LOOKUP_TABLE default\n');
fwrite(Output_file, [Data'],'float','b');
fclose(Output_file);
end
