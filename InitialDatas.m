%%======================================================================%%
% 		Matlab code associated to the publication		%% 
%									%%
%      	A mathematical model to investigate the key drivers of		%%
%	the biogeography of the colon microbiota.     			%%
%       Authors : Simon Labarthe ,Bastien Polizzi, Thuy Phan, 		%%
%	Thierry Goudon, Magali Ribot, Beatrice Laroche    		%%
%                      					                %%
%%======================================================================%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Content: This file contains the initialisation of the unknowns	%							%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Loading intial state, computed from a steady state with a daily amount of fibre of 20g.day-1, no peristaltism and no chemotaxis
load('InitialDataWithMicrobiota.mat')




% Initialisation of the matrix for the dissolved compounds :
%-----------------------------------------------------------

%%%%% Starting from steady-state
if Nb.dissolved >= 1
    for k=1:Nb.dissolved
        InitialData.(['S_',ID.dissolved_idx{k}])=S(:,:,k);
    end
end



% Initialisation of the matrix for the mixture cmponents :
%---------------------------------------------------------


%%%%%% Starting from scratch. Those lines can be uncommented (and the following ones commented) to start from scratch (empty gut, with only water and mucus, and invasion through the inflow).
%if Nb.phases >=1
%	for k=1:Nb.phases
%	        InitialData.(['C_',ID.phase_idx{k}])=zeros(Nz,Nr);
%        end
%        InitialData.C_m=repmat(Initpar.M0R,Nz,1);
%end

%%%%% Starting from steady-state
if Nb.phases >=1
	for k=1:Nb.phases
	        InitialData.(['C_',ID.phase_idx{k}])=C(:,:,k);
        end	
end


% Initialisation of the matrix for the velocity fields :
%-------------------------------------------------------
InitialData.Vr=zeros(Nz,Nr+1);
InitialData.Vz=zeros(Nz+1,Nr);


% Extraction of the names of the unknowns :
%------------------------------------------
InitialDataNames=fieldnames(InitialData);


