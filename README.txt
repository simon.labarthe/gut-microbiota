%%======================================================================%%
%			Gut-Microbiota v1				%%
% 		Copyright © - INRA/IMFT/Université d’Orléans		%%
% 		/CNRS/Université Côte d’Azur/Inria – 2018		%%
%									%%
%									%%
% 		Matlab code associated to the publication		%% 
%									%%
%      	A mathematical model to investigate the key drivers of		%%
%	the biogeography of the colon microbiota.     			%%
%									%%
%       Authors : Simon Labarthe ,Bastien Polizzi, Thuy Phan, 		%%
%	Thierry Goudon, Magali Ribot, Beatrice Laroche    		%%
%                      					                %%
%%======================================================================%%


%%======================================================================%%
% 		Matlab code associated to the publication		%% 
%									%%
%      	A mathematical model to investigate the key drivers of		%%
%	the biogeography of the colon microbiota.     			%%
%       Authors : Simon Labarthe ,Bastien Polizzi, Thuy Phan, 		%%
%	Thierry Goudon, Magali Ribot, Beatrice Laroche    		%%
%                      					                %%
%%======================================================================%%


This code is the implementation of the model of the gut microbiota in its intestinal environment introduced in the paper "Biogeography of the gut microbiota: investigating key drivers through a mathematical model" (Labarthe et al., submitted). It must be ran in Matlab (MathWorks, checked with version R2016b) but needs no additionnal toolboxs: all the numerical functions are embedded in the present files.


This code is distributed under the terms of the GNU Lesser General Public License (LGPL: see the LICENCE file and the files COPYING and COPYING.LESSER for the full licence text).

Gut-Microbiota is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License and the GNU Lesser General Public License for more details.

You should have received a copy of the GNU General Public License and the GNU Lesser General Public License along with Gut-Microbiota.  If not, see <http://www.gnu.org/licenses/>.

If you use this software, please cite the publication "A mathematical model to investigate the key drivers of the biogeography of the gut microbiota", S.Labarthe et al.


% Code Execution
%%%%%%%%%%%%%%%%%%%
To run the model, execute the script MainGutMicrobiota.m with the command
>>MainGutMicrobiota

% Code outputs
%%%%%%%%%%%%%%%%%%%%%%%
In the MicrobioteParameters.m file, the variable 'Saving' allows to decide if the output are saved, and the variable 'SaveInterval' stores the timestep (in days) between two snapshots. The snapshots are stored by default in a folder named 'Output'. A mat file is produced for each snapshot, containing the raw datas (the tables C (mixture components) and S (dissolved solutes)). The table C (resp. S) has a size Nz*Nr*Nb.phases (number of points in the z direction, number of point in the r direction, number of phases in the mixture) (resp. Nz*Nr*Nb.dissolved where Nb.dissolved is the number of dissolved elements). Each phase (resp. each element) can be accessed through its Id, which has the form 'Id.element_name' where 'element_name' can be replaced by the elements of the structure ID.phase_idx or ID.dissolved_idx. For example, C(:,:,Id.m) gives the volume fraction map of the mucus and S(:,:,Id.la) gives the concentration of the lactate. A vtk file containing the distribution of each element is also produced: this file can be displayed through scientific visualization softwares such as Paraview (https://www.paraview.org/).


%File contents
%%%%%%%%%%%%%%%%%%%
	* InitialDatas.m 		: matrix allocations and initial condition definition
	* InitialDataWithMicrobiota.mat	: data file containing the "reference state" defined in the publication "Biogeography of the gut microbiota: investigating key drivers through a mathematical model". This reference state is taken by default as initial condition of the model.
	* MainGutMicrobiota.m		: main script of the model.
	* MicrobioteMeshScale.m		: mesh definition for the numerical resolution of the PDEs involved in the model.
	* MicrobioteOperators.m		: assembly of the different matrix involved in the numerical implementation of the mathematical operators of the PDEs.
	* MicrobioteParameters.m	: definition of the model parameters.
	* MicrobioteSaving.m		: routines used to save the current state of the model
	* post_process.m		: routines used for post-processing purpose : creating vtk snapshots of the different model components and compute suitable outputs (time or spatial averages).
	* README.txt			: this present file.
	* speed_field_construction.m	: routines used to compute the speed field. It implements the approximate model introduced in the publication "Biogeography of the gut microbiota: investigating key drivers through a mathematical model".
	* ToVTK.m			: routines to convert matlab data to vtk files.
	* .gitignore			: files and folder that are not versioned by git.
	* LICENCE			: licence of Gut-Microbiota v1
	* COPYING			: text of the GPL licence v3
	* COPYING.LESSER		: text of the LGPL licence v3
