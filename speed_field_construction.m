%%======================================================================%%
% 		Matlab code associated to the publication		%% 
%									%%
%      	A mathematical model to investigate the key drivers of		%%
%	the biogeography of the colon microbiota.     			%%
%       Authors : Simon Labarthe ,Bastien Polizzi, Thuy Phan, 		%%
%	Thierry Goudon, Magali Ribot, Beatrice Laroche    		%%
%                      					                %%
%%======================================================================%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Content: this file contains a function that computes the value of the speed fields through explicit formula %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



function [Uz,Ur,Upsilon] = speed_field_construction(Upar,CHEMPar,Bound,C,S,t)

global Nz Nr dz Nb R Rgut Lgut ID

Uz=zeros(Nz+1,Nr);
Ur=zeros(Nz,Nr+1);


%%%%%%%% Computation of Uz
Mumtmp = Upar.PToVzGrid*Upar.Mu(C,S); %To Vz grid
Mum1 = 1./(Mumtmp);
Lambda = Mum1*Upar.RadIntrRC1;
Kappatmp = Lambda*Upar.RadIntrRC1;
Kappa=repmat(Kappatmp(:,1),1,Nr);

clearvars Kappatmp Mum1 Mumtmp


if isfield(Bound.GammaM,'L') % capital letters for structure in tests
Gmtmp = Bound.GammaM.Vr(Bound.GammaM.L,Bound.GammaM.M,C)+Upar.PerconstRad;
else
Gmtmp = Bound.GammaM.Vr(Bound.GammaM.l,Bound.GammaM.m,Bound.GammaM.pol,Bound.GammaM.bmon,Bound.GammaM.bla,Bound.GammaM.bH2a,Bound.GammaM.bH2m,Bound.GammaM.r,C,Ur)+Upar.PerconstRad;
end
SumGamma = Gmtmp;
SumGammaR=repmat(SumGamma,1,Nr);
tmpIntSumGamma = Upar.LongInt0zC1*SumGammaR;
IntSumGamma = zeros(Nz+1,Nr);
IntSumGamma(2:Nz+1,:) = tmpIntSumGamma;
clearvars SumGamma SumGammaR SumGammaEdge 
%Uzin = repmat(Upar.Uzin,Nz+1,1);
Uper = Upar.PToVzGrid*repmat(Upar.Uper,1,Nr);
Uz = - Lambda./Kappa.*(Rgut*IntSumGamma-Rgut^2*Upar.Uzin+0.5*Rgut^2*Uper)+Uper;


%%%%%%% Computation of Ur
dzUz = LongDiffEdgeToCell(Uz);
SumdzUz = zeros(Nz,Nr+1);
SumdzUz(:,2:Nr+1) = dzUz* Upar.RadInt0rC1;


Upsilon = zeros(Nz,Nr+1,Nb.chemoPotential);
if isfield(CHEMPar,'ChemioPotential_Idx_C')
for i=1:size(CHEMPar.ChemioPotential_Idx_C,2)
    Ccurr = C(:,:,CHEMPar.ChemioPotential_Idx_C(i));
    SumC = Ccurr*Upar.RadInt0rC1;
    Int01Cedges = repmat(SumC(:,Nr),1,Nr+1);
    SumCedges = zeros(Nz,Nr+1);
    SumCedges(:,2:Nr+1) = SumC;
    Upsilon(:,:,i) =-( SumCedges*Upar.InvrE - (1.0/Rgut^2)*Int01Cedges * Upar.rE);
end
end
if isfield(CHEMPar,'ChemioPotential_Idx_S')
for i=1:size(CHEMPar.ChemioPotential_Idx_S,2)
    Ccurr = S(:,:,CHEMPar.ChemioPotential_Idx_S(i));
    SumC = Ccurr*Upar.RadInt0rC1;
    Int01Cedges = repmat(SumC(:,Nr),1,Nr+1);
    SumCedges = zeros(Nz,Nr+1);
    SumCedges(:,2:Nr+1) = SumC;
    Upsilon(:,:,i+size(CHEMPar.ChemioPotential_Idx_C,2)) =-( SumCedges*Upar.InvrE - (1.0/Rgut^2)*Int01Cedges * Upar.rE);
end
end

%%%%%%% Computation of Upsilon
Upsilontemp_vect=zeros(Nz*(Nr+1),Nb.chemoPotential);
for i=1:Nb.chemoPotential
   Upsilontemp_vect(:,i)=reshape(Upsilon(:,:,i),Nz*(Nr+1),1); 
end

Upsilontemp = Upsilontemp_vect*CHEMPar.sensitivity';
Upsilontemp_scattered=zeros(Nz,(Nr+1),Nb.phases);
for i=1:Nb.chemoBacteria
    Upsilontemp_scattered(:,:,CHEMPar.ChemioBacteria_Idx_C(i))=reshape(Upsilontemp(:,i),Nz,Nr+1);
end
Ctemp = CellToEdgeR(C);
Upsilon=Upsilontemp_scattered;
SumUpsilontemp = sum(Upsilontemp_scattered.*Ctemp,3);

Ur = - SumdzUz*Upar.InvrE - SumUpsilontemp;
clearvars dzUz SumdzUz Upsilontemp SumUpsilontemp Cedges SumCedges Int01Cedges




% Linear extrapolation from a grid centered on the cell center to a grid centered
% on the radial edge centers.
function Uout = CellToEdgeR(Uinit)
    if size(Uinit,2)==Nr+1
        Uout=Uinit;
    elseif length(size(Uinit))==3   
        Uout = zeros(Nz,Nr+1,size(Uinit,3));
        Uout(:,1:Nr,:) = 0.5*Uinit;
        Uout(:,2:Nr+1,:) = Uout(:,2:Nr+1,:)+0.5*Uinit;
        Uout(:,1,:) = Uinit(:,1,:);
        Uout(:,Nr+1,:) = Uinit(:,Nr,:);        
    else
        Uout = zeros(Nz,Nr+1);
        Uout(:,1:Nr) = 0.5*Uinit;
        Uout(:,2:Nr+1) = Uout(:,2:Nr+1)+0.5*Uinit;
        Uout(:,1) = Uinit(:,1);
        Uout(:,Nr+1) = Uinit(:,Nr);
    end
end



% Longitudinal differentiation
function Uout = LongDiffEdgeToCell(Uinit)
        Uout = zeros(Nz,Nr);
        Uout(:,:) =  (Uinit(2:Nz+1,:)-Uinit(1:Nz,:))/dz;                                        
end







end


