
%%======================================================================%%
% 		Matlab code associated to the publication		%% 
%									%%
%      	A mathematical model to investigate the key drivers of		%%
%	the biogeography of the colon microbiota.     			%%
%       Authors : Simon Labarthe ,Bastien Polizzi, Thuy Phan, 		%%
%	Thierry Goudon, Magali Ribot, Beatrice Laroche    		%%
%                      					                %%
%%======================================================================%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Content: This file contains the assembly of the operators		%							%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global Upar

%-------------------------------------------------------------------------%
%           Constructing elementary vectors and identity matrix           %
%-------------------------------------------------------------------------%

% Elementary vectors in radial and vertical direction :
% Of 
er.C=ones(Nr,1); %center
ez.C=ones(Nz,1); %center

er.E=ones(Nr+1,1); %edges
ez.E=ones(Nz+1,1); %edges

% Identity matrix in the R and Z directions :
Idr=speye(Nr);      Idz=speye(Nz);

% Identity matrix of size Nr x Nz :
Nrz=Nr*Nz;
Idrz=speye(Nrz);

% Matrix with ones of size Nr :
global IntrR 
IntrR=dr*sparse(triu(ones(Nr,Nr)));



%-------------------------------------------------------------------------%
%          Constructing of the shifts in the radial the direction         %
%-------------------------------------------------------------------------%

% Construction of the shifts in the radial direction with Dirichlet
% boundary conditions :
%----------------------
% Shift in 1D :
Jr1Dm=spdiags(er.C,-1,Nr,Nr);
Jr1Dp=spdiags(er.C,+1,Nr,Nr);
% Shift in 2D :
Jr2Dm=kron(Jr1Dm,Idz);
Jr2Dp=kron(Jr1Dm',Idz);

% Construction of the shifts in the radial direction with Neumann
% boundary conditions :
%----------------------
% Shift in 1D with m :
Jr1Nm=spdiags(er.C,-1,Nr,Nr);
Jr1Nm(1,1)=1;
% Shift in 1D with p :
Jr1Np=spdiags(er.C,1,Nr,Nr);
Jr1Np(Nr,Nr)=1;
% Shift in 2D :
Jr2Nm=kron(Jr1Nm,Idz);
Jr2Np=kron(Jr1Np,Idz);

%-------------------------------------------------------------------------%
%         Constructing of the shifts in the vertical the direction        %
%-------------------------------------------------------------------------%

% Construction of the shifts in the vertical direction with Dirichlet
% boundary conditions :
%----------------------
% Shift in 1D :
Jz1Dm=spdiags(ez.C,-1,Nz,Nz);
% Shift in 2D :
Jz2Dm=kron(Idr,Jz1Dm);
Jz2Dp=kron(Idr,Jz1Dm');

% Construction of the shifts in the vertical direction with Neumann
% boundary conditions :
%----------------------
% Shift in 1D with m :
Jz1Nm=spdiags(ez.C,-1,Nz,Nz);
Jz1Nm(1,1)=1;
% Shift in 1D with p :
Jz1Np=spdiags(ez.C,1,Nz,Nz);
Jz1Np(Nz,Nz)=1;
% Shift in 2D :
Jz2Nm=kron(Idr,Jz1Nm);
Jz2Np=kron(Idr,Jz1Np);

%-------------------------------------------------------------------------%
%                   Operator for the vertical transport                   %
%-------------------------------------------------------------------------%


% Matricial opperator for the transport in horizontal direction :
%----------------------------------------------------------------
TransportZ=spdiags([-ez.C ez.C],0:1,Nz,Nz+1);


% Matricial opperator for the transport in radial direction :
%-------------------------------------------------------------------
% Coputation of the coefficients :
CoeffLeft=(R.edges(1:Nr)./R.centers)';
CoeffRight=(R.edges(2:Nr+1)./R.centers)';
TransportR=spdiags([-CoeffLeft CoeffRight],0:1,Nr,Nr+1);
TransportR = TransportR';


%-------------------------------------------------------------------------%
%                   Operator for the vertical diffusion                   %
%-------------------------------------------------------------------------%

% Laplacian operator in the Z direction :
%----------------------------------------
% z=0  => Dirichlet,
% z=Lz => Neumann
Laplacian1DzDN=spdiags([ez.C -2*ez.C ez.C],-1:1,Nz,Nz);
Laplacian1DzDN(Nz,Nz)=-1;
Laplacian1DzDN=Laplacian1DzDN/(dz*dz);

% Laplacian operator in the Z direction :
%----------------------------------------
% z=0  => Neumann,
% z=Lz => Neumann
Laplacian1DzNN=spdiags([ez.C -2*ez.C ez.C],-1:1,Nz,Nz);
Laplacian1DzNN(1,1)=-1;
Laplacian1DzNN(Nz,Nz)=-1;
Laplacian1DzNN=Laplacian1DzNN/(dz*dz);

%-------------------------------------------------------------------------%
%                    Operator for the radial diffusion                    %
%-------------------------------------------------------------------------%

% Laplacian operator in the R direction :
%----------------------------------------

% Computation of the coefficients :
CoeffDown=(R.edges(1:Nr)./R.centers)';
CoeffDown=CoeffDown/dr/dr;
CoeffUp=(R.edges(2:Nr+1)./R.centers)';
CoeffUp=CoeffUp/dr/dr;

% Laplacian in R with : r=0 => Dirichlet, r=R => Dirichlet
Laplacian1DrDD=diag(CoeffDown)*(Jr1Dm-Idr)+diag(CoeffUp)*(Jr1Dp-Idr);

% Laplacian in R with : r=0 => Neumann, r=R => Dirichlet
Laplacian1DrND=diag(CoeffDown)*(Jr1Nm-Idr)+diag(CoeffUp)*(Jr1Dp-Idr);

% Laplacian in R with : r=0 => Neumann, r=R => Neumann
Laplacian1DrNN=diag(CoeffDown)*(Jr1Nm-Idr)+diag(CoeffUp)*(Jr1Np-Idr);

Laplacian1DrDD=sparse(Laplacian1DrDD);
Laplacian1DrND=sparse(Laplacian1DrND);
Laplacian1DrNN=sparse(Laplacian1DrNN);



%-------------------------------------------------------------------------%
%                 Operator for the chemotactic potentials                 %
%-------------------------------------------------------------------------%
% Initialisation of the chemotactic potential :
%----------------------------------------------
Potential=sparse(Nrz+1,4);
Vchemotactic_r=zeros(Nz,Nr+1,4);
Vchemotactic_z=zeros(Nz+1,Nr,4);

% Laplacian with Neumann boundary conditions and minimisation of mean value:
%---------------------------------------------------------------------------
LapChemo=kron(Idr,Laplacian1DzNN)+kron(Laplacian1DrNN,Idz);
LapChemo(Nrz+1,1:Nrz)=1;
LapChemo(1:Nrz,Nrz+1)=1;
[L_LapChemo,U_LapChemo]=lu(LapChemo);

% Gradient in the r direction in 1D with Neumann boudary conditions :
%--------------------------------------------------------------------
GradChemo1DrNN=spdiags([-er.C er.C],-1:0,Nr+1,Nr);
GradChemo1DrNN(1,1)=0;
GradChemo1DrNN(Nr+1,Nr)=0;
GradChemo1DrNN=GradChemo1DrNN/dr;

% Gradient in the r direction in 2D with Neumann boudary conditions :
%--------------------------------------------------------------------
GradChemo2DrNN=kron(GradChemo1DrNN,Idz);

% Gradient in the z direction in 1D with Neumann boudary conditions :
%--------------------------------------------------------------------
GradChemo1DzNN=spdiags([-ez.C ez.C],-1:0,Nz+1,Nz);
GradChemo1DzNN(1,1)=0;
GradChemo1DzNN(Nz+1,Nz)=0;
GradChemo1DzNN=GradChemo1DzNN/dz;

% Gradient in the z direction in 2D with Neumann boudary conditions :
%--------------------------------------------------------------------
GradChemo2DzNN=kron(Idr,GradChemo1DzNN);

%-------------------------------------------------------------------------%
%               Elementary volume in cilindrical coordinates              %
%-------------------------------------------------------------------------%

ElementaryVolume=dz*pi*(R.GridEdges(1:Nz,2:Nr+1).^2-...
        R.GridEdges(1:Nz,1:Nr).^2);
ElementaryVolume_Liter=ElementaryVolume*(1e-3); % Convertion cm^3 in L.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Construction of material used during the computation of the speed field


%%%% Matrix for radial integration
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%% At centers
% Matrix to compute \int_0^r r X dr, r at cell centers
% result : uper triangular matrix with 2 and ones on the diagonal and the first line, except the first element which is a zero. That gives the coefficient for the trapeze quadrature rule.
% Coeff = (1 2 2 2 ... 2)
%         (0 1 2 2 ... 2)
%         (0 0 1 2 ... 2)
%         (0 0 0 1 ... 2)
%         (      ...    )
%         (0 0 0 0     1)
up_ones =  triu(ones(Nr,Nr));
Coeff = zeros(Nr,Nr) + tril(up_ones) + 2*triu(up_ones,1); % result : uper triangular matrix with 2 and ones on the diagonal
Upar.RadInt0rC  = sparse(0.5* dr *diag(R.centers)*Coeff); 


% Matrix to compute \int_0^r r X dr, r at cell centers, with first order quadrature rule
% Coeff = (1 1 1 1 ... 1)
%         (0 1 1 1 ... 1)
%         (0 0 1 1 ... 1)
%         (0 0 0 1 ... 1)
%         (      ...    )
%         (0 0 0 0     1)
Coeff =  triu(ones(Nr,Nr));
Upar.RadInt0rC1  = sparse(dr *diag(R.centers)*Coeff); 

% Matrix to compute \int_r^R r X dr, r at cell centers
% result : uper triangular matrix with 2 and ones on the diagonal and
% the first line, except the first element which is a zero. That gives the coefficient for the trapeze quadrature rule.
% Coeff = (1 0 0 0 ... 0)
%         (2 1 0 0 ... 0)
%         (2 2 1 0 ... 0)
%         (2 2 2 1 ... 0)
%         (      ...    )
%         (2 2 2 2 ... 1)
up_ones =  tril(ones(Nr,Nr));
Coeff = zeros(Nr,Nr) + triu(up_ones) + 2*tril(up_ones,-1); % result : uper triangular matrix with 2 and ones on the diagonal
Upar.RadIntrRC = sparse(0.5 * dr *diag(R.centers)*Coeff);

% Matrix to compute \int_r^R r X dr, r at cell centers with first order quadrature
% result : uper triangular matrix with 2 and ones on the diagonal and
% the first line, except the first element which is a zero. That gives the coefficient for the trapeze quadrature rule.
% Coeff = (1 0 0 0 ... 0)
%         (1 1 0 0 ... 0)
%         (1 1 1 0 ... 0)
%         (1 1 1 1 ... 0)
%         (      ...    )
%         (1 1 1 1 ... 1)
Coeff =  tril(ones(Nr,Nr));
Upar.RadIntrRC1 = sparse( dr *diag(R.centers)*Coeff);

%%%%%%% At edges
% Matrix to compute \int_0^r r X dr, r at cell edge
% result : uper triangular matrix with 2 and ones on the diagonal and the first line, except the first element which is a zero. That gives the coefficient for the trapeze quadrature rule.
% Coeff = (0 1 1 1 ... 1)
%         (0 1 2 2 ... 2)
%         (0 0 1 2 ... 2)
%         (0 0 0 1 ... 2)
%         (      ...    )
%         (0 0 0 0     1)
up_ones =  triu(ones(Nr+1,Nr+1));
Coeff = zeros(Nr+1,Nr+1) + tril(up_ones) + 2*triu(up_ones,1); % result : uper triangular matrix with 2 and ones on the diagonal
Coeff(1,:)=ones(1,Nr+1);
Coeff(1,1)=0;
Upar.RadInt0rE  = sparse(0.5* dr *diag(R.edges)*Coeff); 

% Matrix to compute \int_r^R r X dr
% result : uper triangular matrix with 2 and ones on the diagonal and
% the first line, except the first element which is a zero. That gives the coefficient for the trapeze quadrature rule.
% Coeff = (1 0 0 0 ... 0)
%         (2 1 0 0 ... 0)
%         (2 2 1 0 ... 0)
%         (2 2 2 1 ... 0)
%         (      ...    )
%         (1 1 1 1 ... 0)
up_ones =  tril(ones(Nr+1,Nr+1));
Coeff = zeros(Nr+1,Nr+1) + triu(up_ones) + 2*tril(up_ones,-1); % result : uper triangular matrix with 2 and ones on the diagonal
Coeff(Nr+1,:)=ones(1,Nr+1);
Coeff(Nr+1,Nr+1)=0;
Upar.RadIntrRE = sparse(0.5 * dr *diag(R.edges)*Coeff);

%%%% Matrix for longitudinal integration
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%% At centers
 
% Matrix to compute \int_O^z  X dz, z at cell centers
% result : uper triangular matrix with 2 and ones on the diagonal and the first line, except the first element which is a zero. That gives the coefficient for the trapeze quadrature rule.
% Coeff = (1 2 2 2 ... 2)
%         (0 1 2 2 ... 2)
%         (0 0 1 2 ... 2)
%         (0 0 0 1 ... 2)
%         (      ...    )
%         (0 0 0 0     1)
up_ones =  triu(ones(Nz,Nz));
Coeff = zeros(Nz,Nz) + tril(up_ones) + 2*triu(up_ones,1); % result : uper triangular matrix with 2 and ones on the diagonal
Upar.LongInt0zC  = sparse(0.5* dz *Coeff');



% Matrix to compute \int_O^z  X dz, z at cell centers with first order quadrature
% Coeff = (1 1 1 1 ... 1)
%         (0 1 1 1 ... 1)
%         (0 0 1 1 ... 1)
%         (0 0 0 1 ... 1)
%         (      ...    )
%         (0 0 0 0     1)
Coeff =  triu(ones(Nz,Nz));
Upar.LongInt0zC1  = sparse(dz *Coeff');

% Matrix to compute \int_z^L  X dz, z at cell centers
% result : uper triangular matrix with 2 and ones on the diagonal and
% the first line, except the first element which is a zero. That gives the coefficient for the trapeze quadrature rule.
% Coeff = (1 0 0 0 ... 0)
%         (2 1 0 0 ... 0)
%         (2 2 1 0 ... 0)
%         (2 2 2 1 ... 0)
%         (      ...    )
%         (2 2 2 2 ... 1)
up_ones =  tril(ones(Nz,Nz));
Coeff = zeros(Nz,Nz) + triu(up_ones) + 2*tril(up_ones,-1); % result : uper triangular matrix with 2 and ones on the diagonal
Upar.LongIntzLC = sparse(0.5 * dz*Coeff');

%%%%%%% At edges

% Matrix to compute \int_0^z X dz, z at cell edge
% result : uper triangular matrix with 2 and ones on the diagonal and the first line, except the first element which is a zero. That gives the coefficient for the trapeze quadrature rule.
% Coeff = (0 1 1 1 ... 1)
%         (0 1 2 2 ... 2)
%         (0 0 1 2 ... 2)
%         (0 0 0 1 ... 2)
%         (      ...    )
%         (0 0 0 0     1)
up_ones =  triu(ones(Nz+1,Nz+1));
Coeff = zeros(Nz+1,Nz+1) + tril(up_ones) + 2*triu(up_ones,1); % result : uper triangular matrix with 2 and ones on the diagonal
Coeff(1,:)=ones(1,Nz+1);
Coeff(1,1)=0;
Upar.LongInt0zE  = sparse(0.5* dz*Coeff');

% Matrix to compute \int_z^L  X dz
% result : uper triangular matrix with 2 and ones on the diagonal and
% the first line, except the first element which is a zero. That gives the coefficient for the trapeze quadrature rule.
% Coeff = (1 0 0 0 ... 0)
%         (2 1 0 0 ... 0)
%         (2 2 1 0 ... 0)
%         (2 2 2 1 ... 0)
%         (      ...    )
%         (1 1 1 1 ... 0)
up_ones =  tril(ones(Nz+1,Nz+1));
Coeff = zeros(Nz+1,Nz+1) + triu(up_ones) + 2*tril(up_ones,-1); % result : uper triangular matrix with 2 and ones on the diagonal
Coeff(Nz+1,:)=ones(1,Nz+1);
Coeff(Nz+1,Nz+1)=0;
Upar.LongIntzLE = sparse(0.5 * dz*Coeff');
clearvars up_ones Coeff    

% Matrix to compute 1/r * A, r being evaluated at the cell centers
Upar.InvrC = sparse(diag(1./R.centers));
Upar.InvrE = zeros(Nr+1);
Upar.InvrE(2:Nr+1,2:Nr+1) = diag(1./R.edges(2:Nr+1));
Upar.InvrE = sparse(Upar.InvrE);
Upar.rC = sparse(diag(R.centers));
Upar.rE = sparse(diag(R.edges));

%%%% Matrix for radial scattering : Right Hand side multiplication
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%From P grid to Vr grid by linear interpolation
tmp_semi = 0.5* ones(Nr,1);
Upar.PToVrGrid = spdiags([tmp_semi,tmp_semi],[0,1],Nr,Nr+1);
Upar.PToVrGrid(1,1) = 1; Upar.PToVrGrid(Nr,Nr+1) = 1;

%From Vr grid to P grid by linear interpolation
tmp_semi = 0.5* ones(Nr,1);
Upar.VrToPGrid = spdiags([tmp_semi,tmp_semi],[-1,0],Nr+1,Nr);

%%%% Matrix for longitudinal scattering : Left Hand Side multiplication
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%From P grid to Vz grid by linear interpolation
tmp_semi = 0.5* ones(Nz,1);
Upar.PToVzGrid = spdiags([tmp_semi,tmp_semi],[-1,0],Nz+1,Nz);
Upar.PToVzGrid(1,1) = 1; Upar.PToVzGrid(Nz+1,Nz) = 1;

%From Vr grid to P grid by linear interpolation
tmp_semi = 0.5* ones(Nz,1);
Upar.VzToPGrid = spdiags([tmp_semi,tmp_semi],[0,1],Nz,Nz+1);

