%%======================================================================%%
% 		Matlab code associated to the publication		%% 
%									%%
%      	A mathematical model to investigate the key drivers of		%%
%	the biogeography of the colon microbiota.     			%%
%       Authors : Simon Labarthe ,Bastien Polizzi, Thuy Phan, 		%%
%	Thierry Goudon, Magali Ribot, Beatrice Laroche    		%%
%                      					                %%
%%======================================================================%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Content: this is the main file of the code.				%	
% To be run in a Matlab environment with the command			%
%	MainGutMicrobiota						%
% Tested with the Matlab version R2016b					%	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%-------------------------------------------------------------------------%
%                     Clear workspace and assignment                      %
%-------------------------------------------------------------------------%

clc
close all
clear all
format short


%-------------------------------------------------------------------------%
%                 Selection of the simulation parameters                  %
%-------------------------------------------------------------------------%


% Size of the domain and number of points :
%------------------------------------------

global Nb Upar CHEMpar Lgut Rgut Nr Nz

% Size of the computational domain (in physical dimensions) :
Lgut=154;         	% Length of the gut [cm]
Rgut=2.5;    		% Radius of the gut [cm]

Lgut_Prox=21;	%Length of the proximal colon [cm]
Lgut_Trans=50;	%Length of the transverse colon [cm]
Lgut_Dist=Lgut-(Lgut_Prox+Lgut_Trans); 	%Length of the distal colon [cm]
Rgut_Lumen=2.3; %Radius of luminal space [cm]
Rgut_mucus=Rgut-Rgut_Lumen; %Thickness of mucus space [cm]


% Number of points in the domain :
Nz=390;      % Number of points for the length of the gut.
Nr=25;      % Number of points for the radius of the gut.

% Number of dissolved biochemical component :
Nb.dissolved=8;
% Number of gazeous biochemical component :
Nb.gazeous=0;
% Number of mixture component
Nb.phases=8;
% Number of methabolic path way in the model :
Nb.process=13;


%--------------------------------------------------------------------
%			Output pathname
%--------------------------------------------------------------------

PathName='Output/Test_Spatial_Microbiota';
mkdir(PathName);
copyfile('MicrobioteParameters.m',PathName)


%-------------------------------------------------------------------------%
%                            Loading data files                           %
%-------------------------------------------------------------------------%

% Loading the mesh
MicrobioteMeshScale

% Loading biophysical parameters:
%--------------------------------------------------------
disp('Loading biophysical parameters...')
MicrobioteParameters



% Loading initial data :
%-----------------------
disp('Loading initial data ...')
InitialDatas

TestNumber=0

% Computation of the operators :
%-----------------------------------------------------------------
MicrobioteOperators


% Initialisation of T0
%-------------------------------------------
t=tinit;


% Initialisation of the vectors variables :
%------------------------------------------
C=zeros(Nz,Nr,Nb.phases);
S=zeros(Nz,Nr,Nb.dissolved);

for k=1:Nb.phases
    C(:,:,k)=InitialData.(['C_',ID.phase_idx{k}]);
end
C(:,:,Nb.phases)=1-sum(C(:,:,1:Nb.phases-1),3); % liquid phase => no equation
for k=1:Nb.dissolved
    S(:,:,k)=InitialData.(['S_',ID.dissolved_idx{k}]);
end
clearvars InitialData

%-------------------------------------------------------------------------%
%          Initialization of the variables for the time loop              %
%-------------------------------------------------------------------------%

% Step number initialisation :
%-----------------------------
StepNumber=0;

% Plot informations :
%--------------------
PlotNumber=0;
CurrentDayPlot=0;

% Save informations :
%--------------------
CurrentDaySave=0;
SaveNumber=0;

% NbSaving
NbSaving=floor(Tf/SaveInterval);


%-------------------------------------------------------------------------%
%                        Save of the initial datas                        %
%-------------------------------------------------------------------------%

if Saving==1
       
    % Display informations on the time loop :
    disp(['********************  SAVE NUMBER : ',num2str(SaveNumber),' ********************'])
    disp(['Current time in days : ',num2str(t)])
    disp(char(10))  % Line break
    
    % Save intermediary data in Matlab format :
    FileName=['Microbiote_T',num2str(TestNumber),'_step=',num2str(SaveNumber),'.mat'];
    name=[PathName,FileName];
    save(name,'C','S','t','StepNumber','dr','dz','R','Z')
    
    % Update of the next time for plot :
    CurrentDaySave=CurrentDaySave+SaveInterval;   
end


flag_stop=0;
iteration_before_stop = 0;

Uz=zeros(Nz+1,Nr);
Ur=zeros(Nz,Nr+1);

%-----------------------------------%
%     Begening of the time loop     %
%-----------------------------------%
tic
while t<Tf
       
    %---------------------------------------------------------------------%
    %              Computation of the boundary conditions                 %
    %---------------------------------------------------------------------%
    % We compute the flux that depend on the current solution on Gamma M
    %boundary condition on Gamma_m and Gamma_out for the elements
    for i=1:Nb.phases-1
	if i==ID.m
		Bound.GammaM.([ID.phase_idx{i},'curr']) = Bound.GammaM.(ID.phase_idx{i})(C);
	else
        	Bound.GammaM.([ID.phase_idx{i},'curr']) = Bound.GammaM.(ID.phase_idx{i})(C,Ur);
	end
    end
    Bound.GammaM.lcurr = Bound.GammaM.l(C); 
    for i=1:Nb.dissolved
        Bound.GammaM.([ID.dissolved_idx{i},'curr']) = Bound.GammaM.(ID.dissolved_idx{i})(S(:,Nr,i));
    end    
    
    %---------------------------------------------------------------------%
    %               Computation of the intestinal velocity                %
    %---------------------------------------------------------------------%    
    [Uz, Ur,Upsilon]=speed_field_construction(Upar,CHEMpar,Bound,C,S,t);
    Vchemotactic_r = Upsilon;
    min_Uz_L=min(Uz(Nz,:));
    %%%%%% Qualitative test on the speed => no negative speed at the output (unphysiological negative speeds can be observed at the output if water absorption is too strong).
    if min_Uz_L<0
          Uz_moy = sum(sum(ElementaryVolume.*(Upar.VzToPGrid*Uz)))/(sum(sum(ones(Nz,Nr))));
 	  max_Uz_L=max(Uz(Nz,:));
 	  Uz_0 = 2*Uz(1,:)*Upar.RadIntrRC1/(Rgut^2);
 	   disp(['Vitesse carac. ',num2str(Uz_moy),' ',num2str(min_Uz_L),' ',num2str(max_Uz_L),' ',num2str(Uz_0(1))]);% print the averaged speed on the domain, minimal, maximal speeds at the output and the averaged speed at the output
    end


    %%%%%%%% Computation of the speed divergence for testing purpose
    Test_Div_U = zeros(Nz,Nr);
    Test_Div_U(:,:) = dz*(Ur(:,2:Nr+1)*diag(R.edges(2:Nr+1))-Ur(:,1:Nr)*diag(R.edges(1:Nr)))...
         + dr*(Uz(2:Nz+1,:)*diag(R.centers)-Uz(1:Nz,:)*diag(R.centers));
    
        
    %---------------------------------------------------------------------%
    %                  Computation of the CFL condition                   %
    %---------------------------------------------------------------------%
    maxEigValz=abs([Uz(:);Upar.Uper(:)]);
    maxEigValr=abs([Vchemotactic_r(:);Ur(:)]);
    for k=1:Nb.phases
        tmpIdx = [Bound.Names{k},'curr'];
	tmpBound=Bound.GammaM.(tmpIdx);
 	maxEigValr=[maxEigValr; abs(tmpBound(:))];
    end
    maxEigValzs=max(maxEigValz(:));
    maxEigValrs=max(maxEigValr(:));
    
    % Computation of the time step :
    dt=min([dr/(1.2*maxEigValrs),dz/(1.2*maxEigValzs),dtmax]);    
    t=t+dt;
    
    %------------------------------------------------------------------------%
    %            Vertical transport : diffusion, speed transport, chemotaxis %
    %------------------------------------------------------------------------%

    posUpUz=Uz<0;% Temporary variable for upstream shift scheme
    posDownUz=Uz>=0; % Temporary variable for upstream shift scheme  
    %%%%%%%%%%%%%%%%%%%%%%%%%
    %% Phases
    %%%%%%%%%%%%%%%%%%%%%%%%%

    CTranspZ = zeros(Nz,Nr,Nb.phases);
    for k=1:Nb.phases-1
        Cup=[C(:,:,k);C(Nz,:,k)]; %temporary value on the boundary : boundary conditions are dealt with later
        Cdown=[C(1,:,k);C(:,:,k)]; %temporary value on the boundary : boundary conditions are dealt with later

        %%%%%%% Computation of the fluxes in the z direction :
        %%% Transport by the fluid
        FluxZ = zeros(Nz+1,Nr);
       	FluxZ(posUpUz)=Uz(posUpUz).*Cup(posUpUz);
       	FluxZ(posDownUz)=Uz(posDownUz).*Cdown(posDownUz);
        %%% Boundary condition at Gamma_in
            FluxZ(1,:) = Uz(1,:).*Bound.GammaIn.(ID.phase_idx{k});  
	%%%%% Boundary condition at Gamma_out 	
	if k~=ID.m % if ID.m => output flux by transport
		FluxZ(Nz+1,:) =Bound.GammaOut.(ID.phase_idx{k})(C,Uz);
	else
		if min(Uz(Nz+1,:))<0
		FluxZ(Nz+1,Uz(Nz+1,:)<0) =Uz(Nz+1,Uz(Nz+1,:)<0).*Bound.GammaIn.(ID.phase_idx{k})(1,Uz(Nz+1,:)<0);% if the outflux is negative (unphysiological situations => a warning is raised after speed computation), we set the flux of mucus equal to the input at the same r.
		end
	end
      
        %%%%%%% Longitudinal transport => no diffusion.
        CTranspZ(:,:,k)=(C(:,:,k)-(dt/dz)*TransportZ*FluxZ);
    end
    
    %%% Update of the phases
    C=CTranspZ;
    C(:,:,Nb.phases)=1-sum(C(:,:,1:Nb.phases-1),3); % liquid phase => no equation


    %%%%%%%%%%%%%%% Positivity and saturation tests     
        if sum(sum(sum(C>1)))>0
        if flag_stop==0
            flag_stop=1;
        else
            iteration_before_stop = iteration_before_stop+1;
        end
        maxtmp=C(C>1);
        disp(['erreur on C: sup 1 Transport Z. Max value :',num2str(max(maxtmp(:))), ' max div :', num2str(max(abs(Test_Div_U(:))))]);
        for Idphase=1:Nb.phases
            if sum(sum(C(:,:,Idphase)>1.0))>0
                disp(['erreur in ',ID.phase_idx{Idphase}])
            end
        end
    end
    if sum(sum(sum(C<0)))>0
        if flag_stop==0
            flag_stop=1;
        else
            iteration_before_stop = iteration_before_stop+1;
        end
        mintmp=C(C<0);
        disp(['erreur on C: inf 0 Transport Z. Min value :',num2str(min(mintmp(:))), ' max div :', num2str(max(abs(Test_Div_U(:))))]);
        for Idphase=1:Nb.phases
            if sum(sum(C(:,:,Idphase)<0))>0
                disp(['erreur in ',ID.phase_idx{Idphase}])
            end
        end
    end
    %%%%%%%%%%%%%%%% End tests


    %%%%%%%%%%%%%%%%%%%%%%%%%
    %% Solutes
    %%%%%%%%%%%%%%%%%%%%%%%%%
    STranspZ = zeros(Nz,Nr,Nb.dissolved);
    for k=1:Nb.dissolved
        Sup=[S(:,:,k);S(Nz,:,k)]; %temporary value on the boundary : boundary conditions are dealt with later
        Sdown=[S(1,:,k);S(:,:,k)]; %temporary value on the boundary : boundary conditions are dealt with later
        %%%%%%% Computation of the fluxes in the z direction :
        %%% Transport by the fluid
        FluxZ = zeros(Nz+1,Nr);
        FluxZ(posUpUz)=Uz(posUpUz).*Sup(posUpUz);
        FluxZ(posDownUz)=Uz(posDownUz).*Sdown(posDownUz);
        
        %%%%%%%%%%%%%%%%Boundary condition at input
        FluxZ(1,:) = Uz(1,:).*Bound.GammaIn.(ID.dissolved_idx{k});
               
        %%%%%%% Longitudinal transport => no diffusion.
        STranspZ(:,:,k)=(S(:,:,k)-(dt/dz)*TransportZ*FluxZ);
    end
    %%% update of the solutes
    S = STranspZ;


    %%%%%%%%%%%%%%%%%% Positivity tests
    if sum(sum(sum(S<0)))>0
        if flag_stop==0
            flag_stop=1;
        else
            iteration_before_stop = iteration_before_stop+1;
        end
        mintmp=S(S<0);
        disp(['erreur on S : transport z : inf 0 . Min value :',num2str(min(mintmp(:))), ' max div :', num2str(max(abs(Test_Div_U(:))))]);
        for Idphase=1:Nb.dissolved
            if sum(sum(S(:,:,Idphase)<0))>0
                disp(['erreur in ',ID.dissolved_idx{Idphase}])
            end
        end
    end
    %%%%%% end of tests


    %---------------------------------------------------------------------%
    %            Radial transport: diffusion, speed transport, chemotaxis %
    %---------------------------------------------------------------------%
    
    posRightUr=Ur<0;
    posLeftUr=Ur>=0;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%
    %% Phases
    %%%%%%%%%%%%%%%%%%%%%%%%%
    
    CTranspR = zeros(Nz,Nr,Nb.phases);
    for k=1:Nb.phases-1
        Cright=[C(:,:,k),C(:,Nr,k)]; %temporary value on the boundary : boundary conditions are dealt with later
        Cleft=[C(:,1,k),C(:,:,k)];%temporary value on the boundary : boundary conditions are dealt with later
        %%%%%%% Computation of the fluxes in the r direction :
        %%% Transport by the fluid
        FluxR=zeros(Nz,Nr+1);
        FluxR(posRightUr)=Ur(posRightUr).*Cright(posRightUr);
        FluxR(posLeftUr)=Ur(posLeftUr).*Cleft(posLeftUr);

        tmpIdx = [Bound.Names{k},'curr'];

        %%%%%%%%%% Boundary conditions
	if k==ID.m
        FluxR(:,Nr+1) = Bound.GammaM.(tmpIdx);
	else
	FluxR(:,Nr+1) = Bound.GammaM.(tmpIdx);
	end
        FluxR(:,1) = 0;
        
        %%% Active transport by chemotaxis
        tmpUrChem=Vchemotactic_r(:,:,k);
        posRightChem=tmpUrChem<0;
        posLeftChem=tmpUrChem>=0;
        FluxR(posRightChem) = FluxR(posRightChem)+tmpUrChem(posRightChem).*Cright(posRightChem);
        FluxR(posLeftChem) = FluxR(posLeftChem)+tmpUrChem(posLeftChem).*Cleft(posLeftChem);
        
        %%%%%%% Computation of the diffusion matrix in the r direction :
        MatDiff=Idr-dt*DiffCoeff.(ID.phase_idx{k})*Laplacian1DrNN;
        
        % Computation of the transport in r direction :
        %----------------------------------------------
        CTranspR(:,:,k)=(MatDiff\((C(:,:,k)-(dt/dr)*FluxR*TransportR)'))';
    end
    %%% Update of the phases
    C=CTranspR;
    C(:,:,Nb.phases)=1-sum(C(:,:,1:Nb.phases-1),3); % liquid phase => no equation
   

    %%%%%% Positivity tests and saturation tests
    if sum(sum(sum(C>1)))>0
        if flag_stop==0
            flag_stop=1;
        else
            iteration_before_stop = iteration_before_stop+1;
        end
        maxtmp=C(C>1);
        disp(['erreur on C: sup 1 Transport r. Max value :',num2str(max(maxtmp(:))), ' max div :', num2str(max(abs(Test_Div_U(:))))]);
    end
    if sum(sum(sum(C<0)))>0
        if flag_stop==0
            flag_stop=1;
        else
            iteration_before_stop = iteration_before_stop+1;
        end
        mintmp=C(C<0);
        disp(['erreur on C: inf 0 Transport r. Min value :',num2str(min(mintmp(:))), ' max div :', num2str(max(abs(Test_Div_U(:))))]);
        for Idphase=1:Nb.phases
            if sum(sum(C(:,:,Idphase)<0))>0
                disp(['erreur in ',ID.phase_idx{Idphase}])
            end
        end
    end
    %%%%%%% end of tests
    
    
     
    %%%%%%%%%%%%%%%%%%%%%%%%%
    %% Solutes
    %%%%%%%%%%%%%%%%%%%%%%%%%
    STranspR = zeros(Nz,Nr,Nb.dissolved);
    %%% Computation of the averaged speed for solutes
    UrAveraged = Ur;

    for i=1:Nb.phases
	    UrAveraged=UrAveraged+Vchemotactic_r(:,:,i).*(C(:,:,i)*Upar.PToVrGrid);
    end
    
    posRightUr=UrAveraged<0;
    PosLeftUr=UrAveraged>=0;
    
    for k=1:Nb.dissolved
        Sright=[S(:,:,k),S(:,Nr,k)]; %temporary value on the boundary : boundary conditions are dealt with later
        Sleft=[S(:,1,k),S(:,:,k)];%temporary value on the boundary : boundary conditions are dealt with later
        %%%%%%% Computation of the fluxes in the r direction :
        %%% Transport by the fluid
        FluxR=zeros(Nz,Nr+1);
        FluxR(posRightUr)=UrAveraged(posRightUr).*Sright(posRightUr);
        FluxR(PosLeftUr)=UrAveraged(PosLeftUr).*Sleft(PosLeftUr);
        %%%%%%%%%% Boundary conditions
        tmpIdx = [ID.dissolved_idx{k},'curr'];
        FluxR(:,Nr+1) = Bound.GammaM.(tmpIdx);
        FluxR(:,1) = 0;
       
        %%%%%%% Computation of the diffusion matrix in the r direction :
        MatDiff=Idr-dt*DiffCoeff.(ID.dissolved_idx{k})*Laplacian1DrNN;
        
        % Computation of the transport in r direction :
        %----------------------------------------------
        STranspR(:,:,k)=(MatDiff\((S(:,:,k)-(dt/dr)*FluxR*TransportR)'))';        
    end
    %%% Update of the solutes
    S=STranspR;

    
    %%%%%%%%%% Positivity tests
    if sum(sum(sum(S<0)))>0
        if flag_stop==0
            flag_stop=1;
        else
            iteration_before_stop = iteration_before_stop+1;
        end
        mintmp=S(S<0);
        disp(['erreur on S : transport r : inf 0 . Min value :',num2str(min(mintmp(:))), ' max div :', num2str(max(abs(Test_Div_U(:))))]);
        for Idphase=1:Nb.dissolved
            if sum(sum(S(:,:,Idphase)<0))>0
                disp(['erreur in ',ID.dissolved_idx{Idphase}])
            end
        end
    end
    %%%% End of tests

    
    
    
    %---------------------------------------------------------------------%
    %            Source function                                          %
    %---------------------------------------------------------------------%
    rho_ce = zeros(Nz,Nr,Nb.process);
    rho_ci = zeros(Nz,Nr,Nb.process);
    rho_si = zeros(Nz,Nr,Nb.process);
    rho_se = zeros(Nz,Nr,Nb.process);

    %%%% Computation of the commun basis for the different versions of rho
    rho_tmp_P1_P6 = zeros(Nz,Nr,6);
    rho_tmp_P1_P6(:,:,ID.P1) = C(:,:,KRpar.Idx_rho_i_num(1))./(KRpar.KxsP(1)*C(:,:,ID.bmon)+C(:,:,KRpar.Idx_rho_i_den(1))+KRpar.epsilon_KR); %KRpar.epsilon_KR=1e-12 is a little parameter to avoid singularities and Na
    rho_tmp_P1_P6(:,:,ID.P2) = C(:,:,KRpar.Idx_rho_i_num(2))./(KRpar.KxsP(2)*C(:,:,ID.bmon)+C(:,:,KRpar.Idx_rho_i_den(2))+KRpar.epsilon_KR);
    for Pidx=3:6
        rho_tmp_P1_P6(:,:,Pidx) = C(:,:,KRpar.Idx_rho_i_num(Pidx))./(KRpar.KxsP(Pidx)+S(:,:,KRpar.Idx_rho_i_den(Pidx)));%.*(C(:,:,ID.m)+C(:,:,ID.l)+C(:,:,ID.pol)); % logistic growth term
    end
    rho_tmp_P1_P6(:,:,6) = rho_tmp_P1_P6(:,:,6).* KRpar.Ph_inib(:,:);
    
    %%%%% Computation of rho_ci
    rho_ci(:,:,ID.P1) = rho_tmp_P1_P6(:,:,ID.P1);
    rho_ci(:,:,ID.P2) = rho_tmp_P1_P6(:,:,ID.P2);
    for Pidx=3:6
        rho_ci(:,:,Pidx) = rho_tmp_P1_P6(:,:,Pidx).*S(:,:,KRpar.Idx_rho_e_mul(Pidx));
    end
    for Pidx=7:10
        rho_ci(:,:,Pidx) = 1;
    end
    
    %%%%% Computation of rho_ce
    for Pidx=3:6
        rho_ce(:,:,Pidx) = rho_ci(:,:,Pidx).*C(:,:,ID.l);
    end
    
    %%%%% Computation of rho_si
    for Pidx=3:6
        rho_si(:,:,Pidx)=rho_tmp_P1_P6(:,:,Pidx).*C(:,:,ID.l);
    end
    for Pidx=11:13
        rho_si(:,:,Pidx) = 1;
    end    
    %%%%% Computation of rho_se
    
    rho_se(:,:,1) = rho_tmp_P1_P6(:,:,1).*C(:,:,KRpar.Idx_rho_e_mul(1));
    rho_se(:,:,2) = rho_tmp_P1_P6(:,:,2).*C(:,:,KRpar.Idx_rho_e_mul(2));
    for Pidx=3:6
        rho_se(:,:,Pidx)=rho_ci(:,:,Pidx).*C(:,:,ID.l);
    end
      
    %% Scattering of tensors rho
    rho_ci_scattered = zeros(Nz*Nr,Nb.process);
    rho_ce_scattered = zeros(Nz*Nr,Nb.process);
    rho_si_scattered = zeros(Nz*Nr,Nb.process);
    rho_se_scattered = zeros(Nz*Nr,Nb.process);
    for i=1:Nb.process
        rho_ci_scattered(:,i) = reshape(rho_ci(:,:,i),Nz*Nr,1);
        rho_ce_scattered(:,i) = reshape(rho_ce(:,:,i),Nz*Nr,1);
        rho_si_scattered(:,i) = reshape(rho_si(:,:,i),Nz*Nr,1);
        rho_se_scattered(:,i) = reshape(rho_se(:,:,i),Nz*Nr,1);
    end
    
    % Multiplication with reacting rates
    Pci_times_rho_ci_scattered = rho_ci_scattered*KRpar.Pci';
    Pce_times_rho_ce_scattered = rho_ce_scattered*KRpar.Pce';
    Psi_times_rho_si_scattered = rho_si_scattered*KRpar.Psi'; 
    Pse_times_rho_se_scattered = rho_se_scattered*KRpar.Pse';
    
    %% Gathering of tensors P*_times_rho_*    
    Pce_times_rho_ce = zeros(Nz,Nr,Nb.phases);
    Pci_times_rho_ci = zeros(Nz,Nr,Nb.phases);
    Pse_times_rho_se = zeros(Nz,Nr,Nb.dissolved); 
    Psi_times_rho_si = zeros(Nz,Nr,Nb.dissolved);
    for i=1:Nb.phases
        Pce_times_rho_ce(:,:,i) = reshape(Pce_times_rho_ce_scattered(:,i),Nz,Nr);
        Pci_times_rho_ci(:,:,i) = reshape(Pci_times_rho_ci_scattered(:,i),Nz,Nr);
    end
    for i=1:Nb.dissolved
        Pse_times_rho_se(:,:,i) = reshape(Pse_times_rho_se_scattered(:,i),Nz,Nr);
        Psi_times_rho_si(:,:,i) = reshape(Psi_times_rho_si_scattered(:,i),Nz,Nr);
    end

    C = (C+dt * Pce_times_rho_ce)./(1+dt*Pci_times_rho_ci);
    S = (S+dt * (Pse_times_rho_se+KRpar.r))./(1+dt*Psi_times_rho_si);
    C(:,:,Nb.phases)=1-sum(C(:,:,1:Nb.phases-1),3); % liquid phase => no equation
    
    
    %---------------------------------------------------------------------%
    %            Test of mass conservation and positivity                 %
    %---------------------------------------------------------------------%
    if sum(sum(sum(C>1)))>0
        if flag_stop==0
            flag_stop=1;
        else
            iteration_before_stop = iteration_before_stop+1;
        end
        maxtmp=C(C>1);
        disp(['erreur on C: sup 1 final. Max value :',num2str(max(maxtmp(:))), ' max div :', num2str(max(abs(Test_Div_U(:))))]);
	break
    end
    if sum(sum(sum(C<0)))>0
        if flag_stop==0
            flag_stop=1;
        else
            iteration_before_stop = iteration_before_stop+1;
        end
        mintmp=C(C<0);
        disp(['erreur on C: inf 0 final. Min value :',num2str(min(mintmp(:))), ' max div :', num2str(max(abs(Test_Div_U(:))))]);
        for Idphase=1:Nb.phases
            if sum(sum(C(:,:,Idphase)<0))>0
                disp(['erreur in ',ID.phase_idx{Idphase}])
                [I,J]=find(C(:,:,Idphase)<0)
            end
        end
	disp('*************************************************************************************************************************')
	disp('*							ERROR : volume conservation issue				      *')	
	disp('*************************************************************************************************************************')
        disp('* Volume conservation could not be respected with the given boundary conditions.                                        *')
        disp('* The simulation ended due to a non-positive volume fraction.                                                           *')
        disp('* If the non positive volume fraction is l, you maybe should reduce the influx of mixture component through the mucosa. *')
	disp('*************************************************************************************************************************')
        break
    end
    if sum(sum(sum(S<0)))>0
        if flag_stop==0
            flag_stop=1;
        else
            iteration_before_stop = iteration_before_stop+1;
        end
        mintmp=S(S<0);
        disp(['erreur on S : inf 0 . Min value :',num2str(min(mintmp(:))), ' max div :', num2str(max(abs(Test_Div_U(:))))]);
        for Idphase=1:Nb.dissolved
            if sum(sum(S(:,:,Idphase)<0))>0
                disp(['erreur in ',ID.dissolved_idx{Idphase}])
            end
        end
	break
    end
       
    
    if t>=CurrentDaySave & Saving==1
        SaveNumber=SaveNumber+1;
        % Run Saving file :
        MicrobioteSaving
        % Update of the next time for plot :
        CurrentDaySave=CurrentDaySave+SaveInterval;
    end
    
    StepNumber=StepNumber+1;
end
totalTime=toc

post_process;

