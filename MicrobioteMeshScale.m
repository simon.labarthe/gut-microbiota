%%======================================================================%%
% 		Matlab code associated to the publication		%% 
%									%%
%      	A mathematical model to investigate the key drivers of		%%
%	the biogeography of the colon microbiota.     			%%
%       Authors : Simon Labarthe ,Bastien Polizzi, Thuy Phan, 		%%
%	Thierry Goudon, Magali Ribot, Beatrice Laroche    		%%
%                      					                %%
%%======================================================================%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Content: Construct the meshes of the different scattered grids	% 
% associated to the computation domain					%	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%-------------------------------------------------------------------------%
%                         Construction of the axes                        %
%-------------------------------------------------------------------------%

global Lgut Rgut Nr Nz
global R Z dr dz

% Computation of the space step :
dr=Rgut/Nr;   			% Space step in the 'r' direction.
dz=Lgut/Nz;                  	% Space step in the 'z' direction.

% Computation of the cell centers in 1D :
R.centers=linspace(dr/2,Rgut-dr/2,Nr);
Z.centers=linspace(dz/2,Lgut-dz/2,Nz);

% Computation of the cell edges in 1D :
R.edges=linspace(0,Rgut,Nr+1);
Z.edges=linspace(0,Lgut,Nz+1);

% Computation of the mesh grid on the cell centers :
[R.grid,Z.grid]=meshgrid(R.centers,Z.centers);

[R.GridEdges,Z.GridEdges]=meshgrid(R.edges,Z.edges);
[R.GridVz,Z.GridVz]=meshgrid(R.centers,Z.edges);
[R.GridVr,Z.GridVr]=meshgrid(R.edges,Z.centers);



