
%%======================================================================%%
% 		Matlab code associated to the publication		%% 
%									%%
%      	A mathematical model to investigate the key drivers of		%%
%	the biogeography of the colon microbiota.     			%%
%       Authors : Simon Labarthe ,Bastien Polizzi, Thuy Phan, 		%%
%	Thierry Goudon, Magali Ribot, Beatrice Laroche    		%%
%                      					                %%
%%======================================================================%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Content: This file contains the biological and physical parameters 	%
% for the model.				%			%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



global KRpar Fpar Upar CHEMpar
global ID 

% Time informations :
%--------------------
tinit=0;            	% Initial time.
dtmax=1e-2;     	% Maximal time step.
Tf=4;      		% Final time.


%Save solution during simulation :
%---------------------------------
Saving=1;              	% Save : 1 or do not Saving : 0.
SaveInterval=1/2;     	% In days

%Recovering solution from a previous point :
%-------------------------------------------
Recovering=0; 		% Computing from a stop point

% Construction of the index:
%---------------------------

%Processes
for id=1:Nb.process
    ID.(['P',num2str(id)])=id;
end
% Phase components
ID.phase_idx={'m','pol','bmon','bla','bH2a','bH2m','r','l'};
for id=1:Nb.phases
    ID.(ID.phase_idx{id})=id;
end
% Bacteria list in phase components
ID.bacteria_idx_in_phase=[3:6];
% Dissolved compounds
ID.dissolved_idx={'mon';'la';'ac';'pro';'but';'CH4';'CO2';'H2'};
for id=1:Nb.dissolved
    ID.(ID.dissolved_idx{id})=id;
end
ID.SCFA = {'ac';'pro';'but'};
ID.SCFA_in_dissolved=[3,4,5];
Nb.SCFA = 3;

% Boundaries
Bound.Names={'m','pol','bmon','bla','bH2a','bH2m','r','l'};
Bound.la_SCFA={'ac','but','pro','la'};


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 			SOURCE FUNCTION					%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Maximal kinetic rates :
%------------------------
Fpar.Tune_coeff=1.0; % tuning parameter for compensation of represion by ID.l, to ensure volume conservation
% param          value                 type                          		unit   ref                symbol in ref
Fpar.kP1 =     0.05* 1.20e3;     %maximum specific hydrolysis rate for mucus,	d-1,    (R.Munoz Tamayo)    k_hyd,z 
Fpar.kP2 =     1.20e3;           %maximum specific hydrolysis rate,  		d-1,    (R.Munoz Tamayo)    k_hyd,z   
Fpar.kP3 =     7.92;             %maximum specific rate subs.cons.,  		d-1,    (R.Munoz Tamayo)    k_m,su  
Fpar.kP4 =     103;              %maximum specific rate subs.cons.,  		d-1,    (R.Munoz Tamayo)    k_m,la
Fpar.kP5 =     108.837;          %maximum specific rate subs.cons.,  		d-1,    (R.Munoz Tamayo)    k_m,H2a
Fpar.kP6 =     22.581;           %maximum specific rate subs.cons.,  		d-1,    (R.Munoz Tamayo)    k_m,H2m
Fpar.kP7 =     0.01;             %bacterial death rate,              		d-1,    (R.Munoz Tamayo)    k_d
Fpar.kP8 =     0.01;             %bacterial death rate,              		d-1,    (R.Munoz Tamayo)    k_d  
Fpar.kP9 =     0.01;             %bacterial death rate,              		d-1,    (R.Munoz Tamayo)    k_d
Fpar.kP10 =    0.01;             %bacterial death rate,              		d-1,    (R.Munoz Tamayo)    k_d
Fpar.kP11 =    200;              %liquid-gaz transfert coef,         		d-1,    (R.Munoz Tamayo)    k_la
Fpar.kP12 =    200;              %liquid-gaz transfert coef,         		d-1,    (R.Munoz Tamayo)    k_la
Fpar.kP13 =    200;              %liquid-gaz transfert coef,         		d-1,    (R.Munoz Tamayo)    k_la

% Convertion constant to convert mol.l^-1 in g.cm^3
Fpar.alpha = 0.113; % Convertion constant	g.mol^{-1}.L.cm^{-3}		(R.Munoz Tamayo)

% Half saturation : conversion were applied to take into account different units between Munoz Tamayo and al. and this study
%------------------
KRpar.HSC1 =29.99*(1000*Fpar.alpha)^(-1);        % Half saturation constant for ratio s/x (Comtois law) [-],  (R.Munoz Tamayo)   K_x,z
KRpar.HSC2 =29.99*(1000*Fpar.alpha)^(-1) ;       % Half saturation constant for ratio s/x (Comtois law) [-],  (R.Munoz Tamayo)   K_x,z
KRpar.HSC3 = 0.0026*10^(-3);      % Half saturation for microbial growth (Monod law)      [Mol.l-1]   (R.Munoz Tamayo)    K_s,su
KRpar.HSC4 =6.626e-3*10^(-3);     % Half saturation for microbial growth (Monod law)      [Mol.l-1]   (R.Munoz Tamayo)    K_s,la
KRpar.HSC5 =1.7e-3*10^(-3);       % Half saturation for microbial growth (Monod law)      [Mol.l-1]   (R.Munoz Tamayo)    K_s,H2a
KRpar.HSC6 = 1.563e-6*10^(-3);    % Half saturation for microbial growth (Monod law)      [Mol.l-1]   (R.Munoz Tamayo)    K_s,H2m


% Gas equilibrium
%------------------
KRpar.Temp=310.15;        % Temperature in Kelvin,
KRpar.R=0.08314;          % Ideal gas constant
KRpar.CH4_P11=0.0011;	
KRpar.CO2_P12=0.0255;
KRpar.H2_P13=7.29e-4;
CG_infty.CH4=1.9106e-10; % We take as [CH4_g]_\infty the asymptotic value of CH4 in the gazeous phase as computed in R.Munoz-Tamayo model. (Version Vadapt-proximal colon)
CG_infty.C02=1.19e-5;    % We take as [C02_g]_\infty the asymptotic value of C02 in the gazeous phase as computed in R.Munoz-Tamayo model. (Version Vadapt-proximal colon)
CG_infty.H2=3.6505e-7;     % We take as [H2_g]_\infty the asymptotic value of H2 in the gazeous phase as computed in R.Munoz-Tamayo model. (Version Vadapt-proximal colon)
Fpar.rP11 = KRpar.CH4_P11*KRpar.R*KRpar.Temp*CG_infty.CH4;   % K_{h,CH4} * R * T * [CH4_g]_\infty g.mol-1.d-1 (R.Munoz Tamayo). 
Fpar.rP12 = KRpar.CO2_P12*KRpar.R*KRpar.Temp*CG_infty.C02;   % K_{h,C02} * R * T * [C02_g]_\infty g.mol-1.d-1 (R.Munoz Tamayo). 
Fpar.rP13 = KRpar.H2_P13 *KRpar.R*KRpar.Temp*CG_infty.H2;   % K_{h,H2}  * R * T * [H2_g]_\infty  g.mol-1.d-1 (R.Munoz Tamayo). 


% Vectorial kinetic rates
%------------------
Fpar.k = [Fpar.kP1;Fpar.kP2;Fpar.kP3;Fpar.kP4;Fpar.kP5;Fpar.kP6;Fpar.kP7;Fpar.kP8;Fpar.kP9;Fpar.kP10;Fpar.kP11;Fpar.kP12;Fpar.kP13];
KRpar.r = zeros(Nz,Nr,Nb.dissolved);
for i = ID.CH4:ID.H2
    KRpar.r(:,:,i)=Fpar.(['rP',num2str(i+5)])*Fpar.(['kP',num2str(i+5)]); %computation of rtilde
end

KRpar.Idx_rho_i_num = [ID.bmon,ID.bmon,ID.bmon,ID.bla,ID.bH2a,ID.bH2m];
KRpar.Idx_rho_i_den = [ID.m,ID.pol,ID.mon,ID.la,ID.H2,ID.H2];
KRpar.Idx_rho_e_mul = [ID.m,ID.pol,ID.mon,ID.la,ID.H2,ID.H2];
KRpar.KxsP = [KRpar.HSC1,KRpar.HSC2,KRpar.HSC3,KRpar.HSC4,KRpar.HSC5,KRpar.HSC6];



% Yield parameters: conversions constant were applied
%------------------
% param          value                 			type                   		unit   	ref             symbol in ref
%P1
Fpar.YmP1  =   1/(1000*Fpar.alpha)    ;		%Yield of component m in process 1,     [-],    (R.Munoz Tamayo)    	Y_su,z
Fpar.YmonP1  = 0.005*(10^3*Fpar.alpha)^(-1);	%Yield of component mon in process 1,   [-],    (R.Munoz Tamayo)    	Y_su,z
%P2
Fpar.YpolP2  =   (10^3*Fpar.alpha)^(-1)    ;	%Yield of component pol in process 2,   [-],    (R.Munoz Tamayo)    	Y_su,z
Fpar.YmonP2  =  0.005*(10^3*Fpar.alpha)^(-1);	%Yield of component mon in process 2,   [-],    (R.Munoz Tamayo)    	Y_su,z 
%P3
Fpar.YmonP3  =   (10^3*Fpar.alpha)^(-1)    ;	%Yield of component mon in process 3,   [-],    (R.Munoz Tamayo)    
Fpar.YbmonP3 =       0.120;               	%biomass yield factor for bacteria metabolizing mon,  [-],    (R.Munoz Tamayo)    Y_su 
Fpar.YlaP3   =0.499*(10^3*Fpar.alpha)^(-1);     %Yield of component la in process 3,                  [-],    (R.Munoz Tamayo)    Y_la,su 
Fpar.YacP3   =   0.567*(10^3*Fpar.alpha)^(-1);  %Yield of component ac in process 3,                  [-],    (R.Munoz Tamayo)    Y_ac,su 
Fpar.YproP3  =   0.240*(10^3*Fpar.alpha)^(-1);  %Yield of component pro in process 3,                 [-],    (R.Munoz Tamayo)    Y_pro,su 
Fpar.YbutP3  =  0.270*(10^3*Fpar.alpha)^(-1);	%Yield of component but in process 3,                 [-],    (R.Munoz Tamayo)    Y_bu,su 
Fpar.YCO2P3  =   1.1*(10^3*Fpar.alpha)^(-1);	%Yield of component CO2 in process 3,                 [-],    (R.Munoz Tamayo)    Y_CO2,su 
Fpar.YH2P3   =    1.440*(10^3*Fpar.alpha)^(-1); %Yield of component H2 in process 3,                  [-],    (R.Munoz Tamayo)    Y_H2,su 
%P4
Fpar.YlaP4   =    (10^3*Fpar.alpha)^(-1)   ;	%Yield of component la in process 4,                  [-],    (R.Munoz Tamayo)   
Fpar.YblaP4  =       0.120;               	%biomass yield factor for bacteria metabolizing la,   [-],    (R.Munoz Tamayo)    Y_la 
Fpar.YacP4   =    0.133*(10^3*Fpar.alpha)^(-1);	%Yield of component ac in process 4,                  [-],    (R.Munoz Tamayo)    Y_ac,la
Fpar.YproP4  =  0.267*(10^3*Fpar.alpha)^(-1);	%Yield of component pro in process 4,                 [-],    (R.Munoz Tamayo)    Y_pro,la 
Fpar.YbutP4  =     0.200*(10^3*Fpar.alpha)^(-1);%Yield of component but in process 4,                 [-],    (R.Munoz Tamayo)    Y_bu,la 
Fpar.YCO2P4  =    0.533*(10^3*Fpar.alpha)^(-1);	%Yield of component CO2 in process 4,                 [-],    (R.Munoz Tamayo)    Y_CO2,la 
Fpar.YH2P4   =    0.400*(10^3*Fpar.alpha)^(-1);	%Yield of component H2 in process 4,                  [-],    (R.Munoz Tamayo)    Y_H2,la
%P5
Fpar.YH2P5   =     (10^3*Fpar.alpha)^(-1)  ;	%Yield of component H2 in process 5,                  [-],    (R.Munoz Tamayo)   
Fpar.YbH2aP5 =       0.043;               	%biomass yield factor for bacteria metabolizing h2 via acetogenesis,   [-],    (R.Munoz Tamayo)    Y_H2a  
Fpar.YacP5   =    0.143*(10^3*Fpar.alpha)^(-1);	%Yield of component ac in process 5,                  [-],    (R.Munoz Tamayo)    Y_ac,H2a
Fpar.YCO2P5  =   -0.5*(10^3*Fpar.alpha)^(-1)  ;	%Yield of component CO2 in process 5,                 [-],    (R.Munoz Tamayo)    Y_CO2,H2a 
%P6
Fpar.YH2P6   =   (10^3*Fpar.alpha)^(-1)    ;	%Yield of component H2 in process 6,                  [-],    (R.Munoz Tamayo)   
Fpar.YbH2mP6 =       0.062;               	%biomass yield factor for bacteria metabolizing h2 via metanogenesis,   [-],    (R.Munoz Tamayo)    Y_H2m
Fpar.YCH4P6  =     0.095*(10^3*Fpar.alpha)^(-1);%Yield of component CH4 in process 6,                  [-],    (R.Munoz Tamayo)    Y_CH4,H2m
Fpar.YCO2P6  =   -0.450*(10^3*Fpar.alpha)^(-1);	%Yield of component CO2 in process 6,                  [-],    (R.Munoz Tamayo)    Y_CO2,H2m



%Computation of Ph repression of metanogenesis
%------------------
Fpar.Phmin = 5.5;                       %Ph in the proximal part of the gut, [-],    (R.Munoz Tamayo)   pH 
Fpar.Phmax = 6.8;                       %Ph in the distal part of the gut,   [-],    (R.Munoz Tamayo)   pH 
Fpar.Ph = repmat(linspace(Fpar.Phmin,Fpar.Phmax,Nz)',1,Nr); % linear repartition of the Ph along the gut
Fpar.Ph_low=5.8;
Fpar.Ph_high=6.7;
KRpar.Ph_inib=exp(-3*((Fpar.Ph-Fpar.Ph_high)/(Fpar.Ph_high-Fpar.Ph_low)).^2).*(Fpar.Ph<Fpar.Ph_high)+(Fpar.Ph>=Fpar.Ph_high);


% Petersen matrix assembly:
%-------------------------------
% Explicit Petersen matrix for phase components 
KRpar.Pce = zeros(Nb.phases,Nb.process);
KRpar.Pce(ID.bmon,ID.P3)=Fpar.YbmonP3;
KRpar.Pce(ID.bla,ID.P4)=Fpar.YblaP4;
KRpar.Pce(ID.bH2a,ID.P5)=Fpar.YbH2aP5;
KRpar.Pce(ID.bH2m,ID.P6)=Fpar.YbH2mP6;
KRpar.Pce = sparse(KRpar.Pce*diag(Fpar.k)); % * diag(k)=> computation of Pce tilde

%Implicit Petersen matrix for phase components
KRpar.Pci = zeros(Nb.phases,Nb.process);
KRpar.Pci(ID.m,ID.P1)=Fpar.YmP1;
KRpar.Pci(ID.pol,ID.P2) = Fpar.YpolP2;
KRpar.Pci(ID.bmon,ID.P7)=1;
KRpar.Pci(ID.bla,ID.P8)=1;
KRpar.Pci(ID.bH2a,ID.P9)=1;
KRpar.Pci(ID.bH2m,ID.P10)=1;
KRpar.Pci=sparse(KRpar.Pci*diag(Fpar.k));% * diag(k)=> computation of Pci tilde

%Explicit Petersen matrix for solutes
KRpar.Pse = zeros(Nb.dissolved,Nb.process);
KRpar.Pse(ID.mon,ID.P1)=Fpar.YmonP1;
KRpar.Pse(ID.mon,ID.P2)=Fpar.YmonP2;
KRpar.Pse(ID.la,ID.P3)=Fpar.YlaP3;
KRpar.Pse(ID.ac,ID.P3)=Fpar.YacP3;
KRpar.Pse(ID.pro,ID.P3)=Fpar.YproP3;
KRpar.Pse(ID.but,ID.P3)=Fpar.YbutP3;
KRpar.Pse(ID.CO2,ID.P3)=Fpar.YCO2P3;
KRpar.Pse(ID.H2,ID.P3)=Fpar.YH2P3;
KRpar.Pse(ID.ac,ID.P4)=Fpar.YacP4;
KRpar.Pse(ID.pro,ID.P4)=Fpar.YproP4;
KRpar.Pse(ID.but,ID.P4)=Fpar.YbutP4;
KRpar.Pse(ID.CO2,ID.P4)=Fpar.YCO2P4;
KRpar.Pse(ID.H2,ID.P4)=Fpar.YH2P4;
KRpar.Pse(ID.ac,ID.P5)=Fpar.YacP5;
KRpar.Pse(ID.CH4,ID.P6)=Fpar.YCH4P6;
KRpar.Pse=sparse(KRpar.Pse*diag(Fpar.k));% * diag(k)=> computation of Pse tilde

%Implicit Petersen matrix for solutes
KRpar.Psi = zeros(Nb.dissolved,Nb.process);
KRpar.Psi(ID.mon,ID.P3) = Fpar.YmonP3;
KRpar.Psi(ID.la,ID.P4) = Fpar.YlaP4;
KRpar.Psi(ID.H2,ID.P5) = Fpar.YH2P5;
KRpar.Psi(ID.CO2,ID.P5)=-Fpar.YCO2P5;
KRpar.Psi(ID.H2,ID.P6) = Fpar.YH2P6;
KRpar.Psi(ID.CO2,ID.P6)=-Fpar.YCO2P6;
KRpar.Psi(ID.CH4,ID.P11) = 1;
KRpar.Psi(ID.CO2,ID.P12) = 1;
KRpar.Psi(ID.H2,ID.P13) = 1;
KRpar.Psi=sparse(KRpar.Psi*diag(Fpar.k));% * diag(k)=> computation of Psi tilde
KRpar.epsilon_KR=1e-12; % little parameter to avoid singularities and NaN

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 			DIFFUSION					%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% param          value                 			type                   		unit   
Fpar.Diffphase = 0.8;	%	Diffusion coefficient for the phase content 		cm^2.d^-1
for i = 1:Nb.phases
	DiffCoeff.(ID.phase_idx{i})=Fpar.Diffphase;
end

DiffCoeff.mon = 1.4;	%	Diffusion coefficient for the monosaccharides		cm^2.d^-1
DiffCoeff.la = 1.4;	%	Diffusion coefficient for the lactate	 		cm^2.d^-1
DiffCoeff.ac = 1.4;	%	Diffusion coefficient for the SCFA	 		cm^2.d^-1
DiffCoeff.pro = 1.4;	%	Diffusion coefficient for the SCFA	 		cm^2.d^-1
DiffCoeff.but = 1.4;	%	Diffusion coefficient for the SCFA	 		cm^2.d^-1
DiffCoeff.H2 = 0.8;	%	Diffusion coefficient for the gas	 		cm^2.d^-1
DiffCoeff.CH4 = 2;	%	Diffusion coefficient for the gas	 		cm^2.d^-1
DiffCoeff.CO2 = 2;	%	Diffusion coefficient for the gas	 		cm^2.d^-1

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 			CHEMOTAXIS					%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%                    
% param          value                 			type                   		unit   
Fpar.communChemoParam=0; %	Chemotactic parameter					cm^2.d^-1, %two orders of magnitude under the ingoing flux
Fpar.Lambda11=Fpar.communChemoParam;
Fpar.Lambda12=Fpar.communChemoParam;
Fpar.Lambda13=Fpar.communChemoParam;
Fpar.Lambda24=Fpar.communChemoParam;
Fpar.Lambda35=Fpar.communChemoParam;
Fpar.Lambda45=Fpar.communChemoParam;


Nb.chemoPotential=5;
Nb.chemoBacteria=4; 
CHEMpar.ChemioBacteria_Idx_C = [ID.bmon,ID.bla,ID.bH2a,ID.bH2m];
CHEMpar.ChemioPotential_Idx_C = [ID.m,ID.pol];%idx in C of the chemotactic attractant : each bacteria is attracted by its metabolite 
CHEMpar.ChemioPotential_Idx_S = [ID.mon,ID.la,ID.H2]; %idx in S of the chemotactic attractant : each bacteria is attracted by its metabolite 
% chemosensitivity matrix : rows (chemotactic bacteria) x columns (chemo attractant. First columns : chemoattractant in C, last columns : chemoattractant in S).
CHEMpar.sensitivity = sparse(... 
	[Fpar.Lambda11, 	Fpar.Lambda12, 	Fpar.Lambda13, 	0, 		0; % bacteria bmon
	0,		0,		0, 		Fpar.Lambda24,  	0; % bacteria bla
	0,		0,		0, 		0,	  	Fpar.Lambda35; % bacteria bH2a
	0,		0,		0, 		0,	  	Fpar.Lambda45]); % bacteria bH2m
CHEMpar.Mucus_sensitivity = [1,1,1,1]; % mucus sensitivity for each bacteria=> tuning the chemiosensitivity with the level of mucus



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 			MUCUS INITIAL CONDITIONS			%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% param          value                 			type                   		unit   
Initpar.MMaxVal = 0.05; % Maximal value for mucus at t0					[-]
Initpar.MMinVal = 0;	% Maximal value for mucus at t0					[-]
Initpar.Mstiffness = 4;	% Stiffness of the sigmoid at t0				[-]
Initpar.Minflexion = 4.6/5*Rgut; % Threshold of sigmoid distribution			[-]
Initpar.M0R=Initpar.MMinVal + (Initpar.MMaxVal-Initpar.MMinVal)*R.centers.^(2*Initpar.Mstiffness)./(R.centers.^(2*Initpar.Mstiffness)+Initpar.Minflexion^(2*Initpar.Mstiffness)); % sigmoid distribution



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 			SPEED PARAMETERS				%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Viscosity parameters : sigmoid model
%----------------------
% param          value                 			type                   		unit 				ref
Upar.ViscosityMin=0.864*10^3;%  water viscosity 					[g.cm^-1.d ^-1]			(Chatelain et al.)
Upar.ViscosityMaxM=73.4*10^3;%  mucus viscosity 					[g.cm^-1.d ^-1]			(Samuel et al.)
Upar.ViscosityMaxBact=35*10^3;% Bacteria viscosity : half mucus viscosity 		[g.cm^-1.d ^-1]			(Samuel et al.) 
Upar.ViscosityMaxPol=70*10^3;% Liquid viscosity with polysaccharide 			[g.cm^-1.d ^-1]	
Upar.ViscosityMaxPol=45*10^3;% [g.cm^-1.d ^-1] Liquid viscosity with polysaccharide 	[g.cm^-1.d ^-1]	
Upar.ViscosityInflexion=8.5/10*Initpar.MMaxVal;% Threshold of sigmoid distribution	[-]
Upar.ViscosityStiffness=7;     	% Stiffness of the sigmoid				[-]

Upar.ViscosityInflexionL=0.5;% Threshold of sigmoid distribution			[-]
Upar.ViscosityStiffnessL=2;  % Stiffness of the sigmoid					[-]

Upar.ViscosityMaxL=0.5*73.4*10^3;% half mucus viscosity					[g.cm^-1.d ^-1] 

Upar.MuM = @(C) Upar.ViscosityMin + (Upar.ViscosityMaxM - Upar.ViscosityMin)*(C.^(2*Upar.ViscosityStiffness)./(Upar.ViscosityInflexion^(2*Upar.ViscosityStiffness)+C.^(2*Upar.ViscosityStiffness)));% sigmoid distribution
Upar.MuL = @(C)Upar.ViscosityMaxL- (Upar.ViscosityMaxL - Upar.ViscosityMin)*(C.^(2*Upar.ViscosityStiffnessL)./(Upar.ViscosityInflexionL^(2*Upar.ViscosityStiffnessL)+C.^(2*Upar.ViscosityStiffnessL)));% sigmoid distribution
Upar.Mu = @(C,S) max(Upar.MuM(C(:,:,ID.m)),Upar.MuL(C(:,:,ID.l)));


% Inflow
%----------------------
% param          value                 			type                   		unit 				ref
Upar.Uin = 1.5*10^3; % Total daily inflow, 						cm^3.day^1		 	(R. Munoz Tamayo)
Upar.Uzin = Upar.Uin/(2*pi*Rgut^2);%Surfacic flux					[cm.day-1].  We divide by 2 because the formula of u_z involves the semi-mean of u_{z,0}. We have : U_in = 1/R^2 \int_0^R r u_z(0,r) dr.



%Peristaltism
%----------------------
% param          value                 			type                   		unit
Upar.Perconst = 0;%		Net effect of gut motility in z direction		cm.day^-1
Upar.PerconstRad = 0;%		Net effect of gut motility in r direction		cm.day^-1
threshold_down=10*dz;%Proximal limit of application of peristaltisl			cm
threshold_up = (Nz-10)*dz;%Distal limit of application of peristaltisl			cm
Upar.Uper = ones(Nz,1)*Upar.Perconst;% peristaltism profile along the gut		cm.day^-1
Upar.Uper(floor(threshold_up/dz):Nz,1)=0;
Upar.Uper(1:floor(threshold_down/dz),1)=0;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 			Boundary conditions				%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Speed
%-------------
%Gamma m
%param          value                 			type                   		unit
Bound.GammaM.Vr = @(fL,fM,fPol,fBmon,fBla,fBH2a,fBH2m,fR,C,Ur) fM(C) + fL(C)+fPol(C,Ur)+fBmon(C,Ur)+fBla(C,Ur)+fBH2a(C,Ur)+fBH2m(C,Ur)+fR(C,Ur);% fonction defining the total flux through gamma_m 	cm.day^-1



%Phase components
%-------------
%Gamma in
% param          value                 			type                   		unit
Bound.GammaIn.(ID.phase_idx{ID.m}) = Initpar.M0R;%Dirichlet boundary condition for m	[-]
for phaseID=2:size(ID.phase_idx,2)-1
    Bound.GammaIn.(ID.phase_idx{phaseID}) = zeros(1,Nr);
end
Bound.GammaIn.(ID.phase_idx{ID.pol})(1,1:Nr) =20/(2*pi*Rgut^2*Upar.Uzin);% Dietary fibre inflow 	[-]    (R. Munoz Tamayo)
Bound.GammaIn.(ID.phase_idx{ID.bmon})(1,1:Nr) =10^-3* 113*5.40e-4;%Dietary bmon flow			[-]    (R. Munoz Tamayo)
Bound.GammaIn.(ID.phase_idx{ID.bla})(1,1:Nr) = 10^-3*113*1.8e-4;% Dietary bla flow			[-]    (R. Munoz Tamayo)
Bound.GammaIn.(ID.phase_idx{ID.bH2a})(1,1:Nr) = 10^-3*113*9e-5;%Dietary bH2a flow			[-]    (R. Munoz Tamayo)
Bound.GammaIn.(ID.phase_idx{ID.bH2m})(1,1:Nr) = 10^-3*113*9e-5;%Dietary bH2m flow			[-]    (R. Munoz Tamayo)
Bound.GammaIn.(ID.phase_idx{ID.r})(1,1:Nr) = 0.05;%Undigestible residuals in the chyme			[-]    (R. Munoz Tamayo)
%Gamma out
% param          value                 			type                   		unit
for phaseID=2:size(ID.phase_idx,2)-1
    Bound.GammaOut.(ID.phase_idx{phaseID}) =@(C,Uz)Uz(Nz+1,:).*C(Nz,:,phaseID);%Output by transport 	[cm.day^-1]
end
%Gamma M
% param          value                 			type                   		unit
Bound.GammaM.m = @(C) 3*(C(:,Nr,ID.m)-Initpar.MMaxVal);%Mucus flux			[cm.d^-1],
Bound.GammaM.pol = @(C,Ur)  zeros(Nz,1);% no flux through the boundary			[cm.d^-1], 
Bound.GammaM.bmon = @(C,Ur) zeros(Nz,1);% no flux through the boundary			[cm.d^-1],
Bound.GammaM.bla = @(C,Ur) zeros(Nz,1);% no flux through the boundary			[cm.d^-1],
Bound.GammaM.bH2a = @(C,Ur) zeros(Nz,1);% no flux through the boundary			[cm.d^-1],
Bound.GammaM.bH2m = @(C,Ur) zeros(Nz,1);% no flux through the boundary			[cm.d^-1],
Bound.GammaM.r = @(C,Ur) zeros(Nz,1);% no flux through the boundary			[cm.d^-1],

Bpar.gammaM.LparProxMin = 17;%Water absorption in the proximal gut			[cm.d^-1],
Bpar.gammaM.LparTransMin = 2;%Water absorption in the distal gut			[cm.d^-1],
Bpar.param_Nprox = floor(1.5*Lgut_Prox/dz);%index of the limit where the distal water absorption is applied
Bpar.PlusGrad=50;% length of the linear variation between proximal and distal water absorption.
Bpar.gammaM.LparMintemp = linspace(Bpar.gammaM.LparProxMin,Bpar.gammaM.LparTransMin,Bpar.PlusGrad)';
Bpar.gammaM.LparMin = Bpar.gammaM.LparProxMin*ones(Nz,1);
Bpar.gammaM.LparMin(Bpar.param_Nprox+1:Bpar.param_Nprox+Bpar.PlusGrad,1) = Bpar.gammaM.LparMintemp(1:Bpar.PlusGrad,1);
Bpar.gammaM.LparMin(Bpar.param_Nprox+1+Bpar.PlusGrad:Nz,1)=Bpar.gammaM.LparTransMin;
Bound.GammaM.l = @(C) Bpar.gammaM.LparMin.*C(:,Nr,ID.l);%water absorption profile 	[cm.d^-1]


%Solutes
%-------------
%Gamma in
% param          value                 			type                   		unit
for soluteID=1:Nb.dissolved
   Bound.GammaIn.(ID.dissolved_idx{soluteID}) = zeros(1,Nr); 
end 
Bound.GammaIn.mon(1,1:Nr) =10^-3*3.333e-2;%Dietary monosaccharides flow			[-]    (R. Munoz Tamayo)

%Gamma M
% param          value                 			type                   		unit		ref
Bpar.gammaM.laProx=(12.60+0.88)*Rgut/2; %Proximal lactate absorption			[cm.d^-1]    (R. Munoz Tamayo)sum of the two values in R.Munoz Tamayo	
Bpar.gammaM.laTrans=(12.60+0.43)*Rgut/2;%Distal lactate absorption			[cm.d^-1]    (R. Munoz Tamayo)sum of the two values in R.Munoz Tamayo
Bpar.gammaM.acProx=(18.90+1.32)*Rgut/2;%Proximal acetate absorption			[cm.d^-1]    (R. Munoz Tamayo)sum of the two values in R.Munoz Tamayo	
Bpar.gammaM.acTrans=(18.90+0.64)*Rgut/2;%Proximal acetate absorption			[cm.d^-1]    (R. Munoz Tamayo)sum of the two values in R.Munoz Tamayo	
Bpar.gammaM.buProx=(12.88+0.90)*Rgut/2;%Proximal butyrate absorption			[cm.d^-1]    (R. Munoz Tamayo)sum of the two values in R.Munoz Tamayo	
Bpar.gammaM.buTrans=(12.88+0.57)*Rgut/2;%Proximal butyrate absorption			[cm.d^-1]    (R. Munoz Tamayo)sum of the two values in R.Munoz Tamayo	
Bpar.gammaM.proProx=(15.32+1.07)*Rgut/2;%Proximal propionate absorption			[cm.d^-1]    (R. Munoz Tamayo)sum of the two values in R.Munoz Tamayo	
Bpar.gammaM.proTrans=(15.32+0.62)*Rgut/2;%Proximal propionate absorption		[cm.d^-1]    (R. Munoz Tamayo)sum of the two values in R.Munoz Tamayo	
Bpar.Prox = [Bpar.gammaM.acProx,Bpar.gammaM.buProx,Bpar.gammaM.proProx,Bpar.gammaM.laProx];%parameter vector
Bpar.Trans = [Bpar.gammaM.acTrans,Bpar.gammaM.buTrans,Bpar.gammaM.proTrans,Bpar.gammaM.laTrans];%parameter vector

for i = 1:Nb.dissolved    
	Bound.GammaM.(ID.dissolved_idx{i}) = @(C) zeros(Nz,1);%Every solute is first given a no-flux boundary condition
end
for i = 1:size(Bound.la_SCFA,2)
        Bpar.tmp = linspace(Bpar.Prox(i),Bpar.Trans(i),Nz)';% Linear variation between proximal and distal values
	Bound.GammaM.(Bound.la_SCFA{i}) = @(C) Bpar.tmp.*C; %lactate and SCFA absorption profile [cm.d^-1]
end

